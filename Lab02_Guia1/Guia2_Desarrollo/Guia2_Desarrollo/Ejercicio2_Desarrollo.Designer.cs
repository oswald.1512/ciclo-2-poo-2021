﻿
namespace Guia2_Desarrollo
{
    partial class Ejercicio2_Desarrollo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbn1 = new System.Windows.Forms.RadioButton();
            this.rbn3 = new System.Windows.Forms.RadioButton();
            this.rbn2 = new System.Windows.Forms.RadioButton();
            this.txtDato1 = new System.Windows.Forms.TextBox();
            this.lblRespuesta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCalcular
            // 
            this.btnCalcular.BackColor = System.Drawing.Color.Transparent;
            this.btnCalcular.BackgroundImage = global::Guia2_Desarrollo.Properties.Resources.budget;
            this.btnCalcular.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCalcular.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.ForeColor = System.Drawing.Color.Transparent;
            this.btnCalcular.Location = new System.Drawing.Point(110, 274);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(189, 128);
            this.btnCalcular.TabIndex = 40;
            this.btnCalcular.UseVisualStyleBackColor = false;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei UI", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(255, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(320, 39);
            this.label6.TabIndex = 39;
            this.label6.Text = "Conversión de datos ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbn1);
            this.groupBox1.Controls.Add(this.rbn3);
            this.groupBox1.Controls.Add(this.rbn2);
            this.groupBox1.Location = new System.Drawing.Point(462, 116);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox1.Size = new System.Drawing.Size(344, 286);
            this.groupBox1.TabIndex = 38;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de conversión";
            // 
            // rbn1
            // 
            this.rbn1.AutoSize = true;
            this.rbn1.Location = new System.Drawing.Point(64, 63);
            this.rbn1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.rbn1.Name = "rbn1";
            this.rbn1.Size = new System.Drawing.Size(260, 21);
            this.rbn1.TabIndex = 10;
            this.rbn1.TabStop = true;
            this.rbn1.Text = "Temperatura (de celcius a farenheit)";
            this.rbn1.UseVisualStyleBackColor = true;
            // 
            // rbn3
            // 
            this.rbn3.AutoSize = true;
            this.rbn3.Location = new System.Drawing.Point(64, 195);
            this.rbn3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.rbn3.Name = "rbn3";
            this.rbn3.Size = new System.Drawing.Size(213, 21);
            this.rbn3.TabIndex = 12;
            this.rbn3.TabStop = true;
            this.rbn3.Text = "Peso (de kilogramos a libras)";
            this.rbn3.UseVisualStyleBackColor = true;
            // 
            // rbn2
            // 
            this.rbn2.AutoSize = true;
            this.rbn2.Location = new System.Drawing.Point(64, 127);
            this.rbn2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.rbn2.Name = "rbn2";
            this.rbn2.Size = new System.Drawing.Size(203, 21);
            this.rbn2.TabIndex = 11;
            this.rbn2.TabStop = true;
            this.rbn2.Text = "Longitud (de metros a pies)";
            this.rbn2.UseVisualStyleBackColor = true;
            // 
            // txtDato1
            // 
            this.txtDato1.Location = new System.Drawing.Point(183, 77);
            this.txtDato1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.txtDato1.Name = "txtDato1";
            this.txtDato1.Size = new System.Drawing.Size(236, 22);
            this.txtDato1.TabIndex = 37;
            this.txtDato1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDato1_KeyPress);
            // 
            // lblRespuesta
            // 
            this.lblRespuesta.AutoSize = true;
            this.lblRespuesta.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRespuesta.Location = new System.Drawing.Point(22, 179);
            this.lblRespuesta.Name = "lblRespuesta";
            this.lblRespuesta.Size = new System.Drawing.Size(0, 23);
            this.lblRespuesta.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 17);
            this.label1.TabIndex = 35;
            this.label1.Text = "Ingresar unidad inicial: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Ejercicio2_Desarrollo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 444);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtDato1);
            this.Controls.Add(this.lblRespuesta);
            this.Controls.Add(this.label1);
            this.Name = "Ejercicio2_Desarrollo";
            this.Text = "Ejercicio2_Desarrollo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbn1;
        private System.Windows.Forms.RadioButton rbn3;
        private System.Windows.Forms.RadioButton rbn2;
        private System.Windows.Forms.TextBox txtDato1;
        private System.Windows.Forms.Label lblRespuesta;
        private System.Windows.Forms.Label label1;
    }
}