﻿
namespace Guia2_Desarrollo
{
    partial class Ejercicio4_Desarrollo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnCalculos = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txbCalculo1 = new System.Windows.Forms.TextBox();
            this.txbCalculo4 = new System.Windows.Forms.TextBox();
            this.txbCalculo2 = new System.Windows.Forms.TextBox();
            this.txtCalculo3 = new System.Windows.Forms.TextBox();
            this.txbNumero = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listbArreglo = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCalculos);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txbCalculo1);
            this.groupBox1.Controls.Add(this.txbCalculo4);
            this.groupBox1.Controls.Add(this.txbCalculo2);
            this.groupBox1.Controls.Add(this.txtCalculo3);
            this.groupBox1.Location = new System.Drawing.Point(286, 76);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(603, 277);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OPERACIONES CON ARREGLO";
            // 
            // btnCalculos
            // 
            this.btnCalculos.BackColor = System.Drawing.Color.Transparent;
            this.btnCalculos.BackgroundImage = global::Guia2_Desarrollo.Properties.Resources.lupa;
            this.btnCalculos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCalculos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalculos.ForeColor = System.Drawing.Color.Transparent;
            this.btnCalculos.Location = new System.Drawing.Point(497, 46);
            this.btnCalculos.Name = "btnCalculos";
            this.btnCalculos.Size = new System.Drawing.Size(88, 78);
            this.btnCalculos.TabIndex = 9;
            this.btnCalculos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCalculos.UseVisualStyleBackColor = false;
            this.btnCalculos.Click += new System.EventHandler(this.btnCalculos_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(188, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Mayor de los pares positivos";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Promedio de impares positivos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(219, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Porcentaje de ceros en el arreglo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(226, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Número mayor de pares negativos";
            // 
            // txbCalculo1
            // 
            this.txbCalculo1.Location = new System.Drawing.Point(253, 46);
            this.txbCalculo1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbCalculo1.Name = "txbCalculo1";
            this.txbCalculo1.ReadOnly = true;
            this.txbCalculo1.Size = new System.Drawing.Size(225, 22);
            this.txbCalculo1.TabIndex = 3;
            // 
            // txbCalculo4
            // 
            this.txbCalculo4.Location = new System.Drawing.Point(253, 222);
            this.txbCalculo4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbCalculo4.Name = "txbCalculo4";
            this.txbCalculo4.ReadOnly = true;
            this.txbCalculo4.Size = new System.Drawing.Size(225, 22);
            this.txbCalculo4.TabIndex = 6;
            // 
            // txbCalculo2
            // 
            this.txbCalculo2.Location = new System.Drawing.Point(253, 102);
            this.txbCalculo2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbCalculo2.Name = "txbCalculo2";
            this.txbCalculo2.ReadOnly = true;
            this.txbCalculo2.Size = new System.Drawing.Size(225, 22);
            this.txbCalculo2.TabIndex = 4;
            // 
            // txtCalculo3
            // 
            this.txtCalculo3.Location = new System.Drawing.Point(253, 160);
            this.txtCalculo3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCalculo3.Name = "txtCalculo3";
            this.txtCalculo3.ReadOnly = true;
            this.txtCalculo3.Size = new System.Drawing.Size(225, 22);
            this.txtCalculo3.TabIndex = 5;
            // 
            // txbNumero
            // 
            this.txbNumero.Location = new System.Drawing.Point(286, 28);
            this.txbNumero.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbNumero.Name = "txbNumero";
            this.txbNumero.Size = new System.Drawing.Size(138, 22);
            this.txbNumero.TabIndex = 11;
            this.txbNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txbNumero_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Ingrese un valor al arreglo";
            // 
            // listbArreglo
            // 
            this.listbArreglo.FormattingEnabled = true;
            this.listbArreglo.ItemHeight = 16;
            this.listbArreglo.Location = new System.Drawing.Point(43, 84);
            this.listbArreglo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listbArreglo.Name = "listbArreglo";
            this.listbArreglo.Size = new System.Drawing.Size(203, 260);
            this.listbArreglo.TabIndex = 9;
            // 
            // Ejercicio4_Desarrollo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 393);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txbNumero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listbArreglo);
            this.Name = "Ejercicio4_Desarrollo";
            this.Text = "Ejercicio4_Desarrollo";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalculos;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbCalculo1;
        private System.Windows.Forms.TextBox txbCalculo4;
        private System.Windows.Forms.TextBox txbCalculo2;
        private System.Windows.Forms.TextBox txtCalculo3;
        private System.Windows.Forms.TextBox txbNumero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listbArreglo;
    }
}