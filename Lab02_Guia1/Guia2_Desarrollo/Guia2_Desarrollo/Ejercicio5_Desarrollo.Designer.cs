﻿
namespace Guia2_Desarrollo
{
    partial class Ejercicio5_Desarrollo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Ejercicio4 = new System.Windows.Forms.Button();
            this.Ejercicio3 = new System.Windows.Forms.Button();
            this.Ejercicio2 = new System.Windows.Forms.Button();
            this.Ejercicio1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(146, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 31);
            this.label1.TabIndex = 4;
            this.label1.Text = "Menú";
            // 
            // Ejercicio4
            // 
            this.Ejercicio4.BackgroundImage = global::Guia2_Desarrollo.Properties.Resources.dato;
            this.Ejercicio4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ejercicio4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio4.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio4.Location = new System.Drawing.Point(221, 243);
            this.Ejercicio4.Name = "Ejercicio4";
            this.Ejercicio4.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio4.TabIndex = 3;
            this.Ejercicio4.UseVisualStyleBackColor = true;
            this.Ejercicio4.Click += new System.EventHandler(this.Ejercicio4_Click);
            // 
            // Ejercicio3
            // 
            this.Ejercicio3.BackgroundImage = global::Guia2_Desarrollo.Properties.Resources.calculador;
            this.Ejercicio3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ejercicio3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio3.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio3.Location = new System.Drawing.Point(12, 243);
            this.Ejercicio3.Name = "Ejercicio3";
            this.Ejercicio3.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio3.TabIndex = 2;
            this.Ejercicio3.UseVisualStyleBackColor = true;
            this.Ejercicio3.Click += new System.EventHandler(this.Ejercicio3_Click);
            // 
            // Ejercicio2
            // 
            this.Ejercicio2.BackgroundImage = global::Guia2_Desarrollo.Properties.Resources.estadisticas;
            this.Ejercicio2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ejercicio2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio2.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio2.Location = new System.Drawing.Point(221, 77);
            this.Ejercicio2.Name = "Ejercicio2";
            this.Ejercicio2.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio2.TabIndex = 1;
            this.Ejercicio2.UseVisualStyleBackColor = true;
            this.Ejercicio2.Click += new System.EventHandler(this.Ejercicio2_Click);
            // 
            // Ejercicio1
            // 
            this.Ejercicio1.BackgroundImage = global::Guia2_Desarrollo.Properties.Resources.etiqueta;
            this.Ejercicio1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Ejercicio1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ejercicio1.ForeColor = System.Drawing.Color.Transparent;
            this.Ejercicio1.Location = new System.Drawing.Point(12, 77);
            this.Ejercicio1.Name = "Ejercicio1";
            this.Ejercicio1.Size = new System.Drawing.Size(133, 106);
            this.Ejercicio1.TabIndex = 0;
            this.Ejercicio1.UseVisualStyleBackColor = true;
            this.Ejercicio1.Click += new System.EventHandler(this.Ejercicio1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 23);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ejercicio 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 23);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ejercicio 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(214, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = "Ejercicio 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(217, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 23);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ejercicio 4";
            // 
            // Ejercicio5_Desarrollo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 369);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Ejercicio4);
            this.Controls.Add(this.Ejercicio3);
            this.Controls.Add(this.Ejercicio2);
            this.Controls.Add(this.Ejercicio1);
            this.Name = "Ejercicio5_Desarrollo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ejercicio5_Desarrollo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Ejercicio1;
        private System.Windows.Forms.Button Ejercicio2;
        private System.Windows.Forms.Button Ejercicio3;
        private System.Windows.Forms.Button Ejercicio4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
    }
}