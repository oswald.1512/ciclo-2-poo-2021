﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2_Desarrollo
{
    public partial class Ejercicio1_Desarrollo : Form
    {
        public Ejercicio1_Desarrollo()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //Validando variables
            double salarioBruto = 0.0, montoDescuento = 0.0, salarioNeto = 0.0;
            //Validando el ingreso de datos
            if (txtBruto.Text.Length == 0 || txtNombre.Text.Length == 0 || txtApellido.Text.Length == 0)
            {
                MessageBox.Show("Debe ingresar todos los campos", "Alerta");

            }
            else
            {
                salarioBruto = Convert.ToDouble(txtBruto.Text);

                //Validando la asignación del cargo
                if (rbn1.Checked == true)
                {
                    montoDescuento = 0.2;
                    salarioNeto = salarioBruto - (salarioBruto * montoDescuento);
                    txtDescuento.Text = Convert.ToString((salarioBruto * montoDescuento));
                    txtNeto.Text = salarioNeto.ToString();

                }
                else if (rbn2.Checked == true)
                {
                    montoDescuento = 0.15;
                    salarioNeto = salarioBruto - (salarioBruto * montoDescuento);
                    txtDescuento.Text = Convert.ToString((salarioBruto * montoDescuento));
                    txtNeto.Text = salarioNeto.ToString();

                }
                else if (rbn3.Checked == true)
                {
                    montoDescuento = 0.05;
                    salarioNeto = salarioBruto - (salarioBruto * montoDescuento);
                    txtDescuento.Text = Convert.ToString((salarioBruto * montoDescuento));
                    txtNeto.Text = salarioNeto.ToString();
                }
                else
                {
                    MessageBox.Show("Debe seleccionar un cargo", "Alerta");
                }
            }
        }

        private void txtBruto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y sus decimales
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                MessageBox.Show("Solo se permiten números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un punto decimal", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            //condicion para solo números
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            //para backspace
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            //para que admita tecla de espacio
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            //si no cumple nada de lo anterior que no lo deje pasar
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se admiten letras", "validación de texto",
               MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            //condicion para solo números
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            //para backspace
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            //para que admita tecla de espacio
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            //si no cumple nada de lo anterior que no lo deje pasar
            else
            {
                e.Handled = true;
                MessageBox.Show("Solo se admiten letras", "validación de texto",
               MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
