﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2_Desarrollo
{
    public partial class Ejercicio2_Desarrollo : Form
    {
        public Ejercicio2_Desarrollo()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            Double dato1, dato2;
            //Validando el ingreso de datos
            if (txtDato1.Text.Length == 0)
            {
                MessageBox.Show("Debe ingresar el campo", "Alerta");

            }
            else
            {
                //Se leen los datos
                dato1 = Convert.ToDouble(txtDato1.Text);

                //Validando la asignación del cargo
                if (rbn1.Checked == true)
                {

                    //Se obtienen los grados fahrenheit 
                    dato2 = (dato1 * (9.0 / 5.0)) + 32;
                    //Se muestra los grados farhenheit resultantes
                    lblRespuesta.Text = dato1.ToString() + "° grados Celsius son " + dato2.ToString() + "° grado Fahrenheit";


                }
                else if (rbn2.Checked == true)
                {
                    dato2 = dato1 * 3.28084;
                    lblRespuesta.Text = dato1.ToString() + " m son igual a " + dato2.ToString() + " pies";


                }
                else if (rbn3.Checked == true)
                {
                    dato2 = dato1 * 2.2;
                    lblRespuesta.Text = dato1.ToString() + " kg son igual a " + dato2.ToString() + " libras";

                }
                else
                {
                    MessageBox.Show("Debe seleccionar una operación", "Alerta");
                }
            }
        }

        private void txtDato1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Validando solo números y sus decimales
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                MessageBox.Show("Solo se permiten números", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
                MessageBox.Show("No se permite más de un punto decimal", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
            }
        }
    }
}
