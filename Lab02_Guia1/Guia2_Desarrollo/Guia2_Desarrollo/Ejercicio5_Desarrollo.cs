﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2_Desarrollo
{
    public partial class Ejercicio5_Desarrollo : Form
    {
        public Ejercicio5_Desarrollo()
        {
            InitializeComponent();
        }

        private void Ejercicio1_Click(object sender, EventArgs e)
        {
            Ejercicio1_Desarrollo Ejercicio = new Ejercicio1_Desarrollo();
            Ejercicio.Show();
        }

        private void Ejercicio2_Click(object sender, EventArgs e)
        {
            Ejercicio2_Desarrollo Ejercicio = new Ejercicio2_Desarrollo();
            Ejercicio.Show();
        }

        private void Ejercicio3_Click(object sender, EventArgs e)
        {
            Ejercicio3_Desarrollo Ejercicio = new Ejercicio3_Desarrollo();
            Ejercicio.Show();
        }

        private void Ejercicio4_Click(object sender, EventArgs e)
        {
            Ejercicio4_Desarrollo Ejercicio = new Ejercicio4_Desarrollo();
            Ejercicio.Show();
        }

        private void ejercicio1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ejercicio1_Desarrollo Ejercicio = new Ejercicio1_Desarrollo();
            Ejercicio.Show();
        }
    }
}
