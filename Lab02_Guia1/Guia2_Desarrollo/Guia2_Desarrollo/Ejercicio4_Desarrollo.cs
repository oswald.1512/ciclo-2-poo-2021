﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia2_Desarrollo
{
    public partial class Ejercicio4_Desarrollo : Form
    {
        public Ejercicio4_Desarrollo()
        {
            InitializeComponent();
        }

        private void btnCalculos_Click(object sender, EventArgs e)
        {
            //Primer cálculo
            double mayorneg = -1000;
            for (int i = 0; i < listbArreglo.Items.Count; i++)
            {
                string valor = listbArreglo.Items[i].ToString();
                double numero = double.Parse(valor);
                if (numero < 0 && numero % 2 == 0)
                {
                    if (numero > mayorneg)

                    {

                        mayorneg = numero;
                        txbCalculo1.Text = mayorneg.ToString();
                    }
                }
                else
                {
                    txbCalculo1.Text = "No hay números negativos pares";
                }
            }
            //Segundo cálculo
            double cantidadnumeros = listbArreglo.Items.Count;
            double cantidadceros = 0;
            double porcentaje = 0;
            for (int i = 0; i < listbArreglo.Items.Count; i++)
            {
                string valor = listbArreglo.Items[i].ToString();
                int numero = int.Parse(valor);
                if (numero == 0)
                {
                    cantidadceros = cantidadceros + 1;
                }
            }
            porcentaje = (cantidadceros / cantidadnumeros) * 100;
            txbCalculo2.Text = porcentaje.ToString() + "%";
            //Tercer cálculo
            double prom;
            double cantidadimpares = 0;
            double suma = 0;
            for (int i = 0; i < listbArreglo.Items.Count; i++)
            {
                string valor = listbArreglo.Items[i].ToString();
                int numero = int.Parse(valor);
                if (numero > 0 && numero % 2 != 0)
                {
                    suma = suma + numero;

                    cantidadimpares = cantidadimpares + 1;

                }
            }
            prom = suma / cantidadimpares;
            txtCalculo3.Text = prom.ToString();

            //Cuarto calculo
            int mayor = 0;
            for (int i = 0; i < listbArreglo.Items.Count; i++)
            {
                string valor = listbArreglo.Items[i].ToString();
                int numero = int.Parse(valor);
                if (numero > 0 && numero % 2 == 0)
                {
                    if (numero > mayor)
                        mayor = numero;
                }
            }
            txbCalculo4.Text = mayor.ToString();
        }
        //Ingresar datos usando la tecla Enter
        private void txbNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                listbArreglo.Items.Add(txbNumero.Text);
                txbNumero.Clear();
                txbNumero.Focus();
            }

        }
    }
}
