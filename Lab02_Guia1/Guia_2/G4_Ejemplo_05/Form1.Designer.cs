﻿
namespace G4_Ejemplo_05
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.listbArreglo = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txbNumero = new System.Windows.Forms.TextBox();
            this.txbCalculo1 = new System.Windows.Forms.TextBox();
            this.txbCalculo2 = new System.Windows.Forms.TextBox();
            this.txtCalculo3 = new System.Windows.Forms.TextBox();
            this.txbCalculo4 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.btnCalc1 = new System.Windows.Forms.Button();
            this.btnCalc2 = new System.Windows.Forms.Button();
            this.btnCalc3 = new System.Windows.Forms.Button();
            this.btnCalc4 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listbArreglo
            // 
            this.listbArreglo.FormattingEnabled = true;
            this.listbArreglo.ItemHeight = 19;
            this.listbArreglo.Location = new System.Drawing.Point(53, 90);
            this.listbArreglo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listbArreglo.Name = "listbArreglo";
            this.listbArreglo.Size = new System.Drawing.Size(203, 270);
            this.listbArreglo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ingrese un valor al arreglo";
            // 
            // txbNumero
            // 
            this.txbNumero.Location = new System.Drawing.Point(296, 34);
            this.txbNumero.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbNumero.Name = "txbNumero";
            this.txbNumero.Size = new System.Drawing.Size(100, 24);
            this.txbNumero.TabIndex = 2;
            // 
            // txbCalculo1
            // 
            this.txbCalculo1.Location = new System.Drawing.Point(253, 46);
            this.txbCalculo1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbCalculo1.Name = "txbCalculo1";
            this.txbCalculo1.ReadOnly = true;
            this.txbCalculo1.Size = new System.Drawing.Size(212, 24);
            this.txbCalculo1.TabIndex = 3;
            // 
            // txbCalculo2
            // 
            this.txbCalculo2.Location = new System.Drawing.Point(253, 102);
            this.txbCalculo2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbCalculo2.Name = "txbCalculo2";
            this.txbCalculo2.ReadOnly = true;
            this.txbCalculo2.Size = new System.Drawing.Size(212, 24);
            this.txbCalculo2.TabIndex = 4;
            // 
            // txtCalculo3
            // 
            this.txtCalculo3.Location = new System.Drawing.Point(253, 160);
            this.txtCalculo3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCalculo3.Name = "txtCalculo3";
            this.txtCalculo3.ReadOnly = true;
            this.txtCalculo3.Size = new System.Drawing.Size(212, 24);
            this.txtCalculo3.TabIndex = 5;
            // 
            // txbCalculo4
            // 
            this.txbCalculo4.Location = new System.Drawing.Point(253, 222);
            this.txbCalculo4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txbCalculo4.Name = "txbCalculo4";
            this.txbCalculo4.ReadOnly = true;
            this.txbCalculo4.Size = new System.Drawing.Size(212, 24);
            this.txbCalculo4.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCalc4);
            this.groupBox1.Controls.Add(this.btnCalc3);
            this.groupBox1.Controls.Add(this.btnCalc2);
            this.groupBox1.Controls.Add(this.btnCalc1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txbCalculo1);
            this.groupBox1.Controls.Add(this.txbCalculo4);
            this.groupBox1.Controls.Add(this.txbCalculo2);
            this.groupBox1.Controls.Add(this.txtCalculo3);
            this.groupBox1.Location = new System.Drawing.Point(296, 82);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(603, 277);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OPERACIONES CON ARREGLO";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(240, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Número mayor de pares negativos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(230, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Porcentaje de ceros en el arreglo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(216, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Promedio de impares positivos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(199, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Mayor de los pares positivos";
            // 
            // btnIngresar
            // 
            this.btnIngresar.Location = new System.Drawing.Point(426, 25);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(107, 43);
            this.btnIngresar.TabIndex = 8;
            this.btnIngresar.Text = "Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // btnCalc1
            // 
            this.btnCalc1.Location = new System.Drawing.Point(497, 46);
            this.btnCalc1.Name = "btnCalc1";
            this.btnCalc1.Size = new System.Drawing.Size(88, 33);
            this.btnCalc1.TabIndex = 9;
            this.btnCalc1.Text = "Mostrar";
            this.btnCalc1.UseVisualStyleBackColor = true;
            this.btnCalc1.Click += new System.EventHandler(this.btnCalc1_Click);
            // 
            // btnCalc2
            // 
            this.btnCalc2.Location = new System.Drawing.Point(497, 102);
            this.btnCalc2.Name = "btnCalc2";
            this.btnCalc2.Size = new System.Drawing.Size(88, 33);
            this.btnCalc2.TabIndex = 11;
            this.btnCalc2.Text = "Mostrar";
            this.btnCalc2.UseVisualStyleBackColor = true;
            this.btnCalc2.Click += new System.EventHandler(this.btnCalc2_Click);
            // 
            // btnCalc3
            // 
            this.btnCalc3.Location = new System.Drawing.Point(497, 160);
            this.btnCalc3.Name = "btnCalc3";
            this.btnCalc3.Size = new System.Drawing.Size(88, 33);
            this.btnCalc3.TabIndex = 12;
            this.btnCalc3.Text = "Mostrar";
            this.btnCalc3.UseVisualStyleBackColor = true;
            this.btnCalc3.Click += new System.EventHandler(this.btnCalc3_Click);
            // 
            // btnCalc4
            // 
            this.btnCalc4.Location = new System.Drawing.Point(497, 222);
            this.btnCalc4.Name = "btnCalc4";
            this.btnCalc4.Size = new System.Drawing.Size(88, 33);
            this.btnCalc4.TabIndex = 13;
            this.btnCalc4.Text = "Mostrar";
            this.btnCalc4.UseVisualStyleBackColor = true;
            this.btnCalc4.Click += new System.EventHandler(this.btnCalc4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(918, 393);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txbNumero);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listbArreglo);
            this.Font = new System.Drawing.Font("Microsoft YaHei UI", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Cálculos Básicos";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listbArreglo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbNumero;
        private System.Windows.Forms.TextBox txbCalculo1;
        private System.Windows.Forms.TextBox txbCalculo2;
        private System.Windows.Forms.TextBox txtCalculo3;
        private System.Windows.Forms.TextBox txbCalculo4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnCalc4;
        private System.Windows.Forms.Button btnCalc3;
        private System.Windows.Forms.Button btnCalc2;
        private System.Windows.Forms.Button btnCalc1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnIngresar;
    }
}

