﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejemplo_02
{
    public partial class frmventana : Form
    {
        public frmventana()
        {
            InitializeComponent();
        }

        public frmventana(string textx)
        {
            InitializeComponent();
            lblRecibido.Text = textx; // Asignamos lo recibido al label
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Form1 form1 = new Form1(); //instanciamos al primer formulario
            this.Close(); //cerramos el formulario actual
            form1.Visible = true; //hacemos visible al form1 de nuevo
        }
    }
}
