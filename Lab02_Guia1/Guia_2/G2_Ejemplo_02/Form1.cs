﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace G2_Ejemplo_02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Agregar item al combo 
            cmbOp.Items.Clear();
            cmbOp.Items.Add("sumar");
            cmbOp.Items.Add("Restar");
           

            //Agregar item a la lista 
            listAdvance.Items.Clear();
            listAdvance.Items.Add("Multiplicacion");
            listAdvance.Items.Add("Division");
            listAdvance.TabIndex = 0;

            //Inhabilitar el combo y la lista
            cmbOp.Enabled = false;
            listAdvance.Enabled = false;
        }

        private void rbtn1_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn1.Checked == true)
            {
                cmbOp.Enabled = true;
                listAdvance.Enabled = false;
            }
        }

        private void rbtn2_CheckedChanged(object sender, EventArgs e)
        {
            cmbOp.Enabled = false;
            listAdvance.Enabled = true;
            //me permite seleccionar el primer elemento de la lista
            listAdvance.SelectedIndex = 0;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            double n1=0, n2=0, r = 0;
            n1 = Convert.ToDouble(txtNum1.Text);
            n2 = Convert.ToDouble(txtNum2.Text);
            if (cmbOp.Enabled == true)
            {
                if (cmbOp.SelectedItem.ToString() == "sumar") 
                    r = n1 + n2;
                else
                    r = n1 - n2;
               // MessageBox.Show("El Resultado es " + r.ToString(), "Respuesta");
            }
            if (listAdvance.Enabled == true)
            {
                if (listAdvance.SelectedItem.ToString() == "Multiplicacion") 
                    r = n1 * n2;
                else
                    r = n1 / n2;
               // MessageBox.Show("El Resultado es " + r.ToString(), "Respuesta");
            }

            string mensaje = string.Format("El Resultado es  " + r.ToString());
            frmventana frmrecibe = new frmventana(mensaje); /* creo un objeto del segundo formulario, adonde mando información*/
            frmrecibe.Visible = true; // muestra el nuevo formulario
            this.Visible = false; // esconde el formulario actual
        }
    }
}
