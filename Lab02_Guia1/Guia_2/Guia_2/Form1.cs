﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia_2
{
    public partial class frmVenta : Form
    {
        public frmVenta()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string nombre = txtNombre.Text;
            MessageBox.Show("Bienvenido a POO " + nombre + " este es tu primer formulario");
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit(); //termina la aplicación
        }
    }
}
