﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Guia5
{
    class Estudiante
    {
        //atributos
        private string carnet;
        private string nombre;
        private DateTime fechaNac;
        private string correo;
        private string responsable;

        //propiedades con validación
        public string Carnet
        {
            get { return carnet; }
            set
            {
                //cadena de expresiones regular para validar carnet
                Regex regexCarnet = new Regex(@"^(([A-Z]){2}([0-9]){6})$");

                carnet = value;
                //Si lo ingresado no coincide lanzo excepción
                if (!regexCarnet.IsMatch(carnet))
                    throw new Exception("¡Formato de carné incorrecto!");
            }
        }
        
        public string Nombre
        {
            get { return nombre; }
            set
            {
                //cadena de expresiones regular para validar nombre
                Regex regexNombre = new Regex(@"[a-zA-ZÁÉÍÓÚñáéíóúÑ]{1,}");

                nombre = value;
                //Si lo ingresado no coincide lanzo excepción
                if (!regexNombre.IsMatch(nombre))
                    throw new Exception("¡Formato de nombre invalido!");
            }
        }

        public DateTime FechaNac
        {
            //Su validación se hizo desde el cs del form
            get { return fechaNac; }
            set { fechaNac = value; }
        }

        public string Correo
        {
            get { return correo; }
            set
            {
                //cadena de expresiones regular para validar correo
                Regex regexCorreo = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$");

                correo = value;
                //Si lo ingresado no coincide lanzo excepción
                if (!regexCorreo.IsMatch(correo))
                    throw new Exception("¡Formato de correo incorrecto!");
            }
        }

        public string Responsable
        {
            get { return responsable; }
            set
            {
                //cadena de expresiones regular para validar nombre
                Regex regexResponsable = new Regex(@"[a-zA-ZÁÉÍÓÚñáéíóúÑ]{1,}");

                responsable = value;
                //Si lo ingresado no coincide lanzo excepción
                if (!regexResponsable.IsMatch(responsable))
                    throw new Exception("¡Formato del nombre del responsable invalido!");
            }
        }
    }
}
