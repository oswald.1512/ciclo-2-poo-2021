﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia5
{
    public partial class G5_Ejemplo_03 : Form
    {
        public G5_Ejemplo_03()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            //tratará de realizar la acción solicitada
            try
            {
                float numera = float.Parse(txtDividendo.Text);
                float denomina = float.Parse(txtDivisor.Text);
                float resultado = numera / denomina;
                txtResultado.Text = Convert.ToString(resultado);
            }
            //si no pudiera hacerlo entonces verificará cual es el error y nos los mostrará
            catch (Exception error)
            {
                MessageBox.Show("El problema es: " + error.Message);
            }
        }

        private void btnReintentar_Click(object sender, EventArgs e)
        {
            txtDividendo.Clear();
            txtDivisor.Clear();
            txtDividendo.Focus();
        }
    }
}
