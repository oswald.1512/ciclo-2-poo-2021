﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia5
{
    public partial class G5_Ejemplo_02 : Form
    {
        public G5_Ejemplo_02()
        {
            InitializeComponent();
        }


        private bool validarCampos()
        {
            //variable que verifica si algo ha sido validado
            bool validado = false;
            if (txtNombre.Text == "") //vefica que no quede vacío el campo
            {
                validado = false; //si está vacío validado es falso
                errorProvider1.SetError(txtNombre, "Ingresar nombre"); //por lo tanto manda a llamar a errorprovider
                //en los parámetros de setError se identifica a quién estoy validando y el mensaje que deseo mandar
            }


            //verifico la casilla de apellido
            if (txtApellido.Text == "")
            {
                validado = false;
                //digo que verifico a txtapellido y si no cumple mando ese mensaje
                errorProvider1.SetError(txtApellido, "Ingrese apellido");
            }

            //verficamos fecha
            //DateTimePicker se llama dtpFechaNacimiento
            DateTime fechaNacimiento = dtpFechaNacimiento.Value;

            //verificamos la fecha del sistema
            int anio = System.DateTime.Now.Year - fechaNacimiento.Year;
            int mes = System.DateTime.Now.Month - fechaNacimiento.Month;
            int dia = System.DateTime.Now.Day - fechaNacimiento.Day;

            /*verificamos si su fecha de nacimiento es hoy ó en el futuro*/
            if (anio == 0 && mes <= 0 && dia <= 0)
            {
                validado = false;
                
                errorProvider1.SetError(dtpFechaNacimiento, "Fecha invalida");
            }
            else
            {
                //si esta bien calculamos la edad
                txtEdad.Text = Convert.ToString(anio);
            }
            
            return validado;
        }

        private void BorrarMesaje()
        {
            //borra los mensajes para que no se muestren y pueda limpiar
            errorProvider1.SetError(txtNombre, "");
            errorProvider1.SetError(txtApellido, "");
            errorProvider1.SetError(dtpFechaNacimiento, "");
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //limpia cualquier mensaje de error de alguna corrida previa
            BorrarMesaje();
            //llamamos al método para validar campos, el de nombre y apellido
            if (validarCampos())
            {
                MessageBox.Show("Los datos se ingresaron correctamente");
            }
        }
    }
}