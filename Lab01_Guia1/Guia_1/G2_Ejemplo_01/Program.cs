﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejemplo_01
{
    class Program
    {
        static Double Potencia(Double x)
        {
            return x * x;
        }

        static void Main(string[] args)
        {
            Console.Title = "Ejercicio potencia con funciones";
            Double x;
            //Se involucra la función
            for (x = 1; x <= 10; x++)
            {
                Console.WriteLine("\n El cuadrado de " + x + " es: " + Potencia(x));
            }
            Console.ReadKey();
        }
    }
}
