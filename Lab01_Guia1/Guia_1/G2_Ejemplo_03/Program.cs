﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejemplo_03
{
    class Program
    {
        static void Faren()
        {
            Double cel, far;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Celsius: ");

            //Se leen los grados celsius y se convierten a double 
            cel = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados fahrenheit 
            far = cel * 0.9 / 5.0 + 32;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Celsiua son {1} grado Fahrenheit", cel, far);

        }
        static void Celsi()
        {
            Double cel, far;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Fahrenheit: ");

            //Se leen los grados celsius y se convierten a double 
            far = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados fahrenheit 
            cel = (far - 32) * 5.0 / 9.0;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Fahrenheit  son {1} grado Celsiua ", far, cel);


        }
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            string opc;

            Console.WriteLine("\t \t PROGRAMA PARA CONVERTIR TEMPERATURAS");
            Console.WriteLine("\n Elige el tipo de conversion");
            Console.WriteLine("\n \n Presiona F si quieres convertir Celsius a fahrenheit");
            Console.WriteLine("\n \n Presiona C si quieres convertir fahrenheit a Celsius");
            Console.WriteLine("\n Elige F ó C: ");

            opc = Console.ReadLine();
            switch (opc)
            {
                case "F":
                case "f":
                    Console.WriteLine("\n Conversion a Fahrenheit");
                    Faren();
                    break;

                case "C":
                case "c":
                    Console.WriteLine("\n Conversion a Celsius");
                    Celsi();
                    break;
                default:
                    Console.WriteLine("\n Tipo de conversión no válida");
                    break;
            }
            Console.ReadKey();
        }
    }
}
