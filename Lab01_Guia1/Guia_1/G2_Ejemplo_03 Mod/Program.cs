﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejemplo_03_Mod
{
    class Program
    {
        //Procedimiemto para pasar de Celsius a Fahrenheit
        static void Faren()
        {
            Double cel, far;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Celsius: ");

            //Se leen los grados celsius y se convierten a double 
            cel = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados fahrenheit 
            far = cel * 0.9 / 5.0 + 32;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Celsius son {1} grado Fahrenheit", cel, far);

        }
        //Procedimiemto para pasar de Fahrenheit a Celsius 
        static void Celsi()
        {
            Double cel, far;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Fahrenheit: ");

            //Se leen los grados fahrenheit y se convierten a double 
            far = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados celsius 
            cel = (far - 32) * 5.0 / 9.0;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Fahrenheit  son {1} grado Celsius ", far, cel);


        }
        //Procedimiemto para pasar de Fahrenheit a Kelvin 
        static void FaKel()
        {
            Double kel, far;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Fahrenheit: ");

            //Se leen los grados fahrenheit y se convierten a double 
            far = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados celsius 
            kel = ((far - 32) * (5.0 / 9.0)) + 273.15;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Fahrenheit  son {1} grado Kelvin ", far, kel);


        }
        //Procedimiemto para pasar de Celsius a Kelvin
        static void Kelvin()
        {
            Double cel, kel;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Celsius: ");

            //Se leen los grados celsius y se convierten a double 
            cel = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados fahrenheit 
            kel = cel + 273.15;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Kelvin son {1} grado Celsius ", kel, cel);


        }
        //Procedimiemto para pasar de Kelvin a Celsius 
        static void KelCel()
        {
            Double cel, kel;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Kelvin: ");

            //Se leen los grados Kelvin y se convierten a double 
            kel = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados Celsius 
            cel = kel - 273.15;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Kelvin son {1} grado Celsius ", kel, cel);


        }
        //Procedimiemto para pasar de Kelvin a Fahrenheit 
        static void KelFa()
        {
            Double far, kel;
            //Aparece en pantalla
            Console.Write("\n Escribe los grados Kelvin: ");

            //Se leen los grados Kelvin y se convierten a double 
            kel = Convert.ToDouble(Console.ReadLine());
            //Se obtienen los grados fahrenheit 
            far = ((kel - 273.15) * (9.0 / 5.0)) + 32;
            //Se muestra los grados farhenheit resultantes
            Console.WriteLine("\n {0} grados Kelvin son {1} grado Fahrenheit ", kel, far);


        }
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            string opc;

            Console.WriteLine("\t \t PROGRAMA PARA CONVERTIR TEMPERATURAS");
            Console.WriteLine("\n Elige el tipo de conversion");
            Console.WriteLine("\n \n Presiona F si quieres convertir Celsius a fahrenheit");
            Console.WriteLine("\n \n Presiona K si quieres convertir Celsius a Kelvin");
            Console.WriteLine("\n \n Presiona C si quieres convertir fahrenheit a Celsius");
            Console.WriteLine("\n \n Presiona O si quieres convertir fahrenheit a Kelvin");
            Console.WriteLine("\n \n Presiona P si quieres convertir Kelvin a Celsius");
            Console.WriteLine("\n \n Presiona Q si quieres convertir Kelvin a fahrenheit");

            Console.WriteLine("\n Elige F , K, C, O, P ó Q : ");

            opc = Console.ReadLine();
            switch (opc)
            {
                case "F":
                case "f":
                    Console.WriteLine("\n Conversion a Fahrenheit");
                    Faren();
                    break;

                case "C":
                case "c":
                    Console.WriteLine("\n Conversion a Celsius");
                    Celsi();
                    break;

                case "K":
                case "k":
                    Console.WriteLine("\n Conversion a Kelvin");
                    Kelvin();
                    break;

                case "O":
                case "o":
                    Console.WriteLine("\n Conversion a Kelvin");
                    FaKel();
                    break;
                case "P":
                case "p":
                    Console.WriteLine("\n Conversion a Celsius");
                    KelCel();
                    break;
                case "Q":
                case "q":
                    Console.WriteLine("\n Conversion a fahrenheit");
                    KelFa();
                    break;

                default:
                    Console.WriteLine("\n Tipo de conversión no válida");
                    break;
            }
            Console.ReadKey();
        }
    }
}
