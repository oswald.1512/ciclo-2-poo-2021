﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejercicio_03
{
    class Program
    {
        
        //Valida que un número sea distinto de 0
        static bool ValidarCero(Double num)
        {
            if(num != 0)
            {
                return true;
            }
            else
            {
                return false;
            }   
        }

        //Procedimiento para dividir un número
        static void Dividir()
        {
            Double dividendo, divisor;
            Console.Write("\n Escribe el dividendo: ");
            dividendo = Convert.ToDouble(Console.ReadLine());

            Console.Write("\n Escribe el divisor: ");
            divisor = Convert.ToDouble(Console.ReadLine());
            while(ValidarCero(divisor)==false)
            {
                Console.Write("\n No se puede dividir entre cero ");
                Console.Write("\n Escribe el divisor nuevamente: ");
                divisor = Convert.ToDouble(Console.ReadLine());
            }

            double division = dividendo / divisor;
            Console.WriteLine("\n El resultado es: " + division);

        }
        //Procedimiento para elevar un número al cubo
        static void Cubo()
        {
            Double bases;

            Console.Write("\n Escribe el número a elevar: ");

            bases = Convert.ToDouble(Console.ReadLine());

            Double potencia = Math.Pow(bases, 3);

            Console.WriteLine("\n " + bases + " elevado al cubo es " + potencia);


        }
        //Procedimiento para calcular el IMC
        static void IMC()
        {
            Double peso, altura, imc;

            Console.Write("\n Ingrese su peso en [Kg]: ");
            peso = Convert.ToDouble(Console.ReadLine());
            while (ValidarCero(peso) == false)
            {
                Console.Write("\n Debe ingresar un valor distinto de cero ");
                Console.Write("\n Escribe el peso nuevamente: ");
                peso = Convert.ToDouble(Console.ReadLine());
            }
            Console.Write("\n Escribe su altura en [m]: ");
            altura = Convert.ToDouble(Console.ReadLine());
            while (ValidarCero(altura) == false)
            {
                Console.Write("\n  Debe ingresar un valor distinto de cero ");
                Console.Write("\n Escribe la altura nuevamente: ");
                altura = Convert.ToDouble(Console.ReadLine());
            }
            imc = peso / Math.Pow(altura, 2);

            Console.WriteLine("\n Su IMC es: " + imc);


        }


        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            string opc;

            Console.WriteLine("\t \t PROGRAMA PARA REALIZAR OPERACIONES");
            Console.WriteLine("\n Elige el tipo de operaciones");
            Console.WriteLine("\n \n Presiona A si quieres Dividir");
            Console.WriteLine("\n \n Presiona B si quieres Obtener cubo");
            Console.WriteLine("\n \n Presiona C si quieres el cálculo de IMC");
            Console.WriteLine("\n \n Presiona D si quieres Salir");

            Console.WriteLine("\n Elige A, B, C ó D ");

            opc = Console.ReadLine();
            switch (opc)
            {
                case "A":
                case "a":
                    Console.WriteLine("\n Dividisión");
                    Dividir();
                    break;

                case "B":
                case "b":
                    Console.WriteLine("\n Obtener cubo");
                    Cubo();
                    break;

                case "C":
                case "c":
                    Console.WriteLine("\n Obtener IMC");
                    IMC();
                    break;

                case "D":
                case "d":
                    Console.WriteLine("\n Gracias por utilizar nuestro sistema");
                    break;

                default:
                    Console.WriteLine("\n Tipo de conversión no válida");
                    break;
            }
            Console.ReadKey();
        }

    }
}
