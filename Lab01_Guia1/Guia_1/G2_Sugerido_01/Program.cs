﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Sugerido_01
{
    class Program
    {
        //Valida que un número sea distinto de 0
        static bool ValidarCero(Double num)
        {
            if (num != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static Double Sucesion(Double n)
        {
            return Math.Pow(-1, n - 1) * (n / Math.Pow(2, n));
        }

        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.Title = "Sucesión matemática";
            Console.WriteLine("\n Ingrese la cantidad de terminos que desea obtener: ");
            int i = int.Parse(Console.ReadLine());
            while (ValidarCero(i) == false)
            {
                Console.Write("\n La cantidad debe ser mayor a cero ");
                Console.Write("\n Ingrese la cantidad nuevamente: ");
                i = int.Parse(Console.ReadLine());
            }
            for (int n = 1; n <= i; n++)
            {
                Console.WriteLine("\n El " + n + ".° término  es: " + Sucesion(n));
            }
            Console.ReadKey();
        }
    }
}
