﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejemplo_04
{
    class Program
    {
        static void Mostrar(Double[] sueldos)
        {
            Console.WriteLine("\n \t Lso sueldos ingresados son: ");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("\t" + sueldos[i]);
            }
        }
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Double[] Sueldos;
            Sueldos = new Double[5];
            for (int i = 0; i < 5; i++)
            {
                int ver = i + 1;
                Console.WriteLine("El sueldo de la secretaria " + ver + " : ");
                String Valor;
                Valor = Console.ReadLine();
                Sueldos[i] = Double.Parse(Valor);
            }
            Mostrar(Sueldos);
            Console.ReadKey();
        }
    }
}
