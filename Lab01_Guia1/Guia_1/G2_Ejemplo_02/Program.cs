﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejemplo_02
{
    class Program
    {
        static void Raiz()
        {
            Console.WriteLine("Calculando raices");
            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("\n La raíz cuadrada del número " + i + " es: " + Math.Sqrt(i));
            }
        }
        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Raiz();
            Console.ReadKey();
        }
    }
}
