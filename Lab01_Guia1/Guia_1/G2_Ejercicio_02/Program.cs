﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace G2_Ejercicio_02
{
    class Program
    {
        //Valida que la edad sea menor al año actual
        static bool ValidarMayorYear(int num)
        {
            DateTime fecha = DateTime.Today;
            if (num > fecha.Year)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Procedimiemto para calcular la edad ingresando año de nacimiento
        static void Years()
        {
            DateTime fecha = DateTime.Today;
            int nacimiento;
            Console.WriteLine("\n Ingrese su año de nacimiento: ");
            nacimiento = int.Parse(Console.ReadLine());
            while (ValidarMayorYear(nacimiento) == true)
            {
                Console.Write("\n Usted no ha nacido ");
                Console.Write("\n Ingrese un valor válido: ");
                nacimiento = int.Parse(Console.ReadLine());
            }
            int year = fecha.Year;
            int edad = year - nacimiento;
            Console.WriteLine("\n Su edad es: " + edad + " años");
        }

        static void Main(string[] args)
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.Title = "Calculo de edad";
            Years();
            Console.ReadKey();
        }
    }
}
