﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_3
{

    public partial class Ejercicio2 : Form
    {

        public Ejercicio2()
        {
            InitializeComponent();            
        }
        

        private void Ejercicio2_Load(object sender, EventArgs e)
        {

           
            gbox1.Enabled = false;
            gbox2.Enabled = false;
            gbox3.Enabled = false;

            //el usuario no podra editar las opciones del cbo
            cboOpcion.DropDownStyle = ComboBoxStyle.DropDownList;


            //lleno de opciones al primer cbo
            cboOpcion.Items.Clear();
            cboOpcion.Items.Add("Ingresar datos");
            cboOpcion.Items.Add("Ver listado");
            cboOpcion.Items.Add("Ver reportes");
        }
        Estudiante student = new Estudiante();

        private void txtTamaño_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }

        
        private void cboOpcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboOpcion.SelectedItem.ToString() == "Ingresar datos")
            {
                gbox1.Enabled = true;                
            }
            else
            {
                gbox1.Enabled = false;                
            }

            if (cboOpcion.SelectedItem.ToString() == "Ver reportes")
            {
                gbox2.Enabled = true;
                //Mostrar reporte de alumnos
                student.mostrar(listMostrar); 
            }
            else
            {
                gbox2.Enabled = false;
                listMostrar.Items.Clear();
            }

            if (cboOpcion.SelectedItem.ToString() == "Ver listado")
            {
                gbox3.Enabled = true;
                //Mostrar datos de estudiantes registrados
                student.listado(listReportes);  
            }
            else
            {
                gbox3.Enabled = false;
                listReportes.Items.Clear();
            }
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {                                   
            student.Llenado(nupCantidad); 
        }

        private void nupCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
