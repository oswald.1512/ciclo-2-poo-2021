﻿namespace Practica_3
{
    partial class Ejercicio2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button6 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.gbox1 = new System.Windows.Forms.GroupBox();
            this.nupCantidad = new System.Windows.Forms.NumericUpDown();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.gbox2 = new System.Windows.Forms.GroupBox();
            this.listMostrar = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cboOpcion = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gbox3 = new System.Windows.Forms.GroupBox();
            this.listReportes = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCantidad)).BeginInit();
            this.gbox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.button6.Location = new System.Drawing.Point(2277, -8);
            this.button6.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(67, 97);
            this.button6.TabIndex = 22;
            this.button6.Text = "X";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.label1.Location = new System.Drawing.Point(40, 73);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 27);
            this.label1.TabIndex = 24;
            this.label1.Text = "Número de alumnos:";
            // 
            // gbox1
            // 
            this.gbox1.Controls.Add(this.nupCantidad);
            this.gbox1.Controls.Add(this.btnRegistrar);
            this.gbox1.Controls.Add(this.label1);
            this.gbox1.Location = new System.Drawing.Point(39, 251);
            this.gbox1.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gbox1.Name = "gbox1";
            this.gbox1.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gbox1.Size = new System.Drawing.Size(626, 148);
            this.gbox1.TabIndex = 27;
            this.gbox1.TabStop = false;
            this.gbox1.Text = "Cantidad a registrar";
            // 
            // nupCantidad
            // 
            this.nupCantidad.Location = new System.Drawing.Point(276, 75);
            this.nupCantidad.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.nupCantidad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupCantidad.Name = "nupCantidad";
            this.nupCantidad.Size = new System.Drawing.Size(114, 27);
            this.nupCantidad.TabIndex = 27;
            this.nupCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nupCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.nupCantidad_KeyPress);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.BackColor = System.Drawing.Color.Transparent;
            this.btnRegistrar.BackgroundImage = global::Practica_3.Properties.Resources.estadisticas;
            this.btnRegistrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegistrar.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.btnRegistrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRegistrar.Location = new System.Drawing.Point(427, 51);
            this.btnRegistrar.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(179, 70);
            this.btnRegistrar.TabIndex = 26;
            this.btnRegistrar.UseVisualStyleBackColor = false;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // gbox2
            // 
            this.gbox2.Controls.Add(this.listMostrar);
            this.gbox2.Location = new System.Drawing.Point(33, 462);
            this.gbox2.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gbox2.Name = "gbox2";
            this.gbox2.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gbox2.Size = new System.Drawing.Size(547, 273);
            this.gbox2.TabIndex = 28;
            this.gbox2.TabStop = false;
            this.gbox2.Text = "Reportes de Notas";
            // 
            // listMostrar
            // 
            this.listMostrar.Font = new System.Drawing.Font("Bookman Old Style", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listMostrar.FormattingEnabled = true;
            this.listMostrar.ItemHeight = 18;
            this.listMostrar.Location = new System.Drawing.Point(12, 71);
            this.listMostrar.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.listMostrar.Name = "listMostrar";
            this.listMostrar.Size = new System.Drawing.Size(523, 148);
            this.listMostrar.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cboOpcion);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(39, 100);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.groupBox3.Size = new System.Drawing.Size(437, 106);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Opciones";
            // 
            // cboOpcion
            // 
            this.cboOpcion.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.cboOpcion.FormattingEnabled = true;
            this.cboOpcion.Location = new System.Drawing.Point(241, 45);
            this.cboOpcion.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.cboOpcion.Name = "cboOpcion";
            this.cboOpcion.Size = new System.Drawing.Size(138, 35);
            this.cboOpcion.TabIndex = 28;
            this.cboOpcion.SelectedIndexChanged += new System.EventHandler(this.cboOpcion_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 12F);
            this.label2.Location = new System.Drawing.Point(40, 45);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 27);
            this.label2.TabIndex = 27;
            this.label2.Text = "Escoja una opción:";
            // 
            // gbox3
            // 
            this.gbox3.Controls.Add(this.listReportes);
            this.gbox3.Location = new System.Drawing.Point(592, 473);
            this.gbox3.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gbox3.Name = "gbox3";
            this.gbox3.Padding = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gbox3.Size = new System.Drawing.Size(435, 262);
            this.gbox3.TabIndex = 29;
            this.gbox3.TabStop = false;
            this.gbox3.Text = "Listado de estudiantes";
            // 
            // listReportes
            // 
            this.listReportes.FormattingEnabled = true;
            this.listReportes.ItemHeight = 21;
            this.listReportes.Location = new System.Drawing.Point(60, 60);
            this.listReportes.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.listReportes.Name = "listReportes";
            this.listReportes.Size = new System.Drawing.Size(319, 151);
            this.listReportes.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bookman Old Style", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(354, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(311, 33);
            this.label4.TabIndex = 30;
            this.label4.Text = "ARREGLO DE NOTAS";
            // 
            // Ejercicio2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1042, 765);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbox3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gbox2);
            this.Controls.Add(this.gbox1);
            this.Controls.Add(this.button6);
            this.Font = new System.Drawing.Font("Bookman Old Style", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "Ejercicio2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ejercicio2";
            this.Load += new System.EventHandler(this.Ejercicio2_Load);
            this.gbox1.ResumeLayout(false);
            this.gbox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nupCantidad)).EndInit();
            this.gbox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.GroupBox gbox1;
        private System.Windows.Forms.GroupBox gbox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cboOpcion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listMostrar;
        private System.Windows.Forms.GroupBox gbox3;
        private System.Windows.Forms.ListBox listReportes;
        private System.Windows.Forms.NumericUpDown nupCantidad;
        private System.Windows.Forms.Label label4;
    }
}