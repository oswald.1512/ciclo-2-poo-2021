﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_3
{
    class Estudiante
    {
        Estudiante[] estudiantes;
        
        string carnet;
        public string Carnet
        {
            get
            {
                return carnet;
            }
            set
            {
                carnet = value;
            }
        }
        string nombre;
        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        string apellido;
        public string Apellido
        {
            get
            {
                return apellido;
            }
            set
            {
                apellido = value;
            }
        }
        string materia;
        public string Materia
        {
            get
            {
                return materia;
            }
            set
            {
                materia = value;
            }
        }

        double nota1;
        public double Nota1
        {
            get
            {
                return  nota1;
            }
            set
            {
                nota1 = value;
            }
        }

        double nota2;
        public double Nota2
        {
            get
            {
                return nota2;
            }
            set
            {
                nota2 = value;
            }
        }

        double nota3;
        public double Nota3
        {
            get
            {
                return nota3;
            }
            set
            {
                nota3 = value;
            }
        }        

        //metodos

        
        public void IngresarDatos()
        {
            carnet = Interaction.InputBox("Ingrese el carné del o la estudiante");
            nombre = Interaction.InputBox("Ingrese el nombre del o la estudiante");
            apellido = Interaction.InputBox("Ingrese el apellido del o la estudiante");
            materia = Interaction.InputBox("Ingrese la materia del o la estudiante");
            nota1 = Convert.ToDouble(Interaction.InputBox("Ingrese la nota 1 del o la estudiante"));
            nota2 = Convert.ToDouble(Interaction.InputBox("Ingrese la nota 2 del o la estudiante"));
            nota3 = Convert.ToDouble(Interaction.InputBox("Ingrese la nota 3 del o la estudiante"));                      
        }

        
        int tamvec;
        public void Llenado(NumericUpDown numericUpDown)
        {                                  
            tamvec = Convert.ToInt32(numericUpDown.Value);
            estudiantes = new Estudiante[tamvec]; //declaro y lleno vector   
            //valido en caso de dejarse vacío el numeric up down            
            if (numericUpDown.Text.Trim() != "")
            {
                for (int i = 0; i < estudiantes.Length; i++)
                {
                    estudiantes[i] = new Estudiante();
                    estudiantes[i].IngresarDatos();
                }
            }
            else
            {
                MessageBox.Show("Ingrese la cantidad correcta");                
            }
        }

        //metodo para imprimir resultados
        public void mostrar(ListBox lista)
        {
            if (estudiantes != null) {  //se valida en caso de estar vacío el arreglo
            for (int j = 0; j < estudiantes.Length; j++)
            {
            lista.Items.Add("El alumno: " + estudiantes[j].Nombre + " " + estudiantes[j].Apellido + " con carné " + estudiantes[j].Carnet
                + " esta desarrollando la materia " + estudiantes[j].Materia + "\n sus notas en esta asignatura son: ");
            lista.Items.Add(estudiantes[j].Nota1 + ", " + estudiantes[j].Nota2 + ", " + estudiantes[j].Nota3);
            double promedio = Math.Round(((estudiantes[j].Nota1 + estudiantes[j].Nota2 + estudiantes[j].Nota3) / 3),2);
            lista.Items.Add("\n Y su promedio es: " + promedio);
            lista.Items.Add("\n");
            } 

            } 
            else
            {
                MessageBox.Show("Verifique la información registrada");
            }
        }
        
        //metodo para mostrar datos de estudiante
        public void listado(ListBox lista)
        {
            if (estudiantes != null)
            {
                for (int i = 0; i < estudiantes.Length; i++)
                {
                    lista.Items.Add("Estudiante número " + (i + 1) + ": ");
                    lista.Items.Add(estudiantes[i].Nombre + " " + estudiantes[i].Apellido);
                    lista.Items.Add("\n");
                }
            }
            else
            {
                MessageBox.Show("No a registrado la información");
            }
        }
    }
}
