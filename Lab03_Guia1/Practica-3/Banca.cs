﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_3
{
    class Banca
    {
        //atributes
        private string dui;
        private string nombre;
        private string apellido;
        private string tipoDeCuenta;
        private string nit;
        private string numeroCuenta;
        private double monto;
        private string sucursal;

        public string Dui { 
            get => dui; 
            set => dui = value; }
        public string Nombres { 
            get => nombre; 
            set => nombre = value; }
        public string Apellidos { 
            get => apellido; 
            set => apellido = value; }
        public string TipoCuenta { 
            get => tipoDeCuenta; 
            set => tipoDeCuenta = value; }
        public string Nit { 
            get => nit; 
            set => nit = value; }
        public string NumeroCuenta { 
            get => numeroCuenta; set =>
                numeroCuenta = value; }
        public double Monto { 
            get => monto; 
            set => monto = value; }
        public string Sucursal { 
            get => sucursal; 
            set => sucursal = value; }
    }
}
