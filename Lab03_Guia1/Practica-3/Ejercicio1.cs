﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Practica_3
{
    public partial class Ejercicio1 : Form
    {
        public Ejercicio1()
        {
            InitializeComponent();
        }

        private List<Banca> Clientes = new List<Banca>(); 
        private int edit_index = -1;

        
        bool validarTxtVacio(GroupBox ejercicio1) 
        {            
            bool empty=false;
            foreach (Control c in ejercicio1.Controls) 
            {
                
                if (c.GetType() == typeof(TextBox) && c.Text == String.Empty)
                {
                    empty = true;
                }                                       
            }

            
            if (empty == true)
            {
                MessageBox.Show("Llena los campos que estén vacios!");
            }

            return empty;
        }

        
        private bool ValidandoMonto(double amount)
        {
            bool var = false;
            if(amount ==0)
            {
                MessageBox.Show("Ingresa un valor diferente de cero!");
                txtMonto.Focus();
                txtMonto.Text = "";
                var = true;
            }
            return var;
        }

        
        private bool ValidateDui(string duiTxt)
        {
            bool var = false;
            
            //formato: ########-#
            Regex dui = new Regex("^[0-9]{8}-[0-9]{1}$");
            if (!dui.IsMatch(duiTxt))
            {
                MessageBox.Show("Formato de DUI incorrecto");
                txtDui.Focus();
                var = true;
            }
            return var;
        }

                       
        private bool ValidateNit(string nitTxt)
        {
            bool var = false;
            
            //format: ####-######-###-#
            Regex nit = new Regex("^[0-9]{4}-[0-9]{6}-[0-9]{3}-[0-9]{1}");
            if (!nit.IsMatch(nitTxt))
            {
                MessageBox.Show("Formato de NIT incorrecto");
                txtNit.Focus();
                var = true;
            }
            return var;                
        }

        
        private bool ValidarCuenta(string type, string number)
        {
            bool var = false;
            switch (type)
            {
                case "Corriente":
                     
                    //formato: CA-00001
                    Regex corriente = new Regex("[(CC)]{2}-[0-9]{5}");
                    if (!corriente.IsMatch(number))
                    {
                        MessageBox.Show("Formato de numero de cuenta incorrecto");
                        txtNumeroCuenta.Focus();
                        var = true;
                    }
                    break;

                case "Ahorros":
                    
                    //formato: CA-00001
                    Regex ahorros = new Regex("[(CA)]{2}-[0-9]{5}");
                    if (!ahorros.IsMatch(number))
                    {
                        MessageBox.Show("Formato de numero de cuenta incorrecto");
                        txtNumeroCuenta.Focus();
                        var = true;
                    }
                    break;

                case "Plazos":
                    
                    //formato: CA-00001
                    Regex plazos = new Regex("[(CP)]{2}-[0-9]{5}");
                    if (!plazos.IsMatch(number))
                    {
                        MessageBox.Show("Formato de numero de cuenta incorrecto");
                        txtNumeroCuenta.Focus();
                        var = true;
                    }
                    break;
            }
            return var;
        }

        private void updateGrid()
        {
            dgvDatos.DataSource = null;
            dgvDatos.DataSource = Clientes; 
        }

        private void delete(GroupBox groupBox)
        {            
            foreach (Control c in groupBox.Controls) 
            {
                
                if (c.GetType() == typeof(TextBox))
                {
                    c.Text = "";
                }
            }
        }
       

        

        private void Ejercicio1_Load(object sender, EventArgs e)
        {
            
            cboTipoCuenta.DropDownStyle = ComboBoxStyle.DropDownList;
            cboSucursal.DropDownStyle = ComboBoxStyle.DropDownList;

            cboTipoCuenta.Items.Clear();
            cboSucursal.Items.Clear();

            //options for cbo's
            cboTipoCuenta.Items.Add("Corriente");
            cboTipoCuenta.Items.Add("Ahorros");
            cboTipoCuenta.Items.Add("Plazos");
            cboTipoCuenta.SelectedIndex = 0; //select option 1 by default

            cboSucursal.Items.Add("Sucursal Rayo");
            cboSucursal.Items.Add("Sucursal Tornado");
            cboSucursal.Items.Add("Sucursal Trueno");
            cboSucursal.Items.Add("Sucursal Tormenta");
            cboSucursal.Items.Add("Sucursal Relámpago");
            cboSucursal.SelectedIndex = 0; //select option 1 by default

        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {            
            if (!validarTxtVacio(groupBox1)) 
            {                
                Banca cliente = new Banca(); 
                //set value to attributes
                cliente.Dui = txtDui.Text.ToString();
                cliente.Nombres = txtNombres.Text.ToString();
                cliente.Apellidos = txtApellido.Text.ToString();
                cliente.TipoCuenta = cboTipoCuenta.Text.ToString();
                cliente.Nit = txtNit.Text.ToString();
                cliente.NumeroCuenta = txtNumeroCuenta.Text.ToString();
                cliente.Monto = Convert.ToDouble(txtMonto.Text);
                cliente.Sucursal = cboSucursal.Text.ToString();

               
                if (!ValidandoMonto(cliente.Monto) && !ValidateDui(cliente.Dui) && !ValidateNit(cliente.Nit) && !ValidarCuenta(cliente.TipoCuenta, cliente.NumeroCuenta))
                {                    
                    
                    if(edit_index > -1)
                    {
                        Clientes[edit_index] = cliente;
                        edit_index = -1;
                    }
                    else
                    {
                        Clientes.Add(cliente);
                    }
                    updateGrid();
                }                                                                          
            }
        }

       

       
        private void txtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

             
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

       

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (edit_index > -1) 
            {
                Clientes.RemoveAt(edit_index);
                edit_index = -1;                
                updateGrid();
            }
            else
            {
                MessageBox.Show("Seleccione con doble click ");
            }
        }

        private void dgvDatos_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewRow seleccion = dgvDatos.SelectedRows[0];
            int posicion = dgvDatos.Rows.IndexOf(seleccion); 
            edit_index = posicion; 

            Banca cliente = Clientes[posicion]; 
            //attributes
            double monto = Convert.ToDouble(txtMonto.Text);
            string dui = txtDui.Text.ToString();
            string nit = txtNit.Text.ToString();
            string tipo = cboTipoCuenta.SelectedItem.ToString();
            string numeroDeLaCuenta = txtNumeroCuenta.Text.ToString();
        }
    }
}
