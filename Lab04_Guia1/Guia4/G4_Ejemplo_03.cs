﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Ejemplo_03 : Form
    {
        public G4_Ejemplo_03()
        {
            InitializeComponent();
        }

        //Función para validar todos los datos dependiendo si me indica que figura es
        private string valida(string indicador)
        {
            //cadena de expresiones regulares donde solo se aceptan números doubles positivos
            Regex regexNumero = new Regex(@"^[0.0-9.9]+$");

            switch (indicador){
                //Si el indicador es el cuadrado
                case "Cuadrado":
                    if (!string.IsNullOrEmpty(txtLado.Text))//Valido si el campo no esta vacío
                    {                        
                        if (regexNumero.IsMatch(txtLado.Text))//Valido si el dato es un número
                        {
                            //En dado caso todo este correcto retorno un 1 como señal
                            return "1";
                        }
                        else
                        {
                            MessageBox.Show("¡Solo se aceptan números positivos!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡Rellene el campo vacío!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case "Circulo":
                    if (!string.IsNullOrEmpty(txtRadio.Text))//Valido si el campo no esta vacío
                    {
                        if (regexNumero.IsMatch(txtRadio.Text))//Valido si el dato es un número
                        {
                            //En dado caso todo este correcto retorno un 1 como señal
                            return "1";
                        }
                        else
                        {
                            MessageBox.Show("¡Solo se aceptan números positivos!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡Rellene el campo vacío!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case "Rombo":
                    if (!string.IsNullOrEmpty(txtMayor.Text) && !string.IsNullOrEmpty(txtMenor.Text))//Valido si los campos no estan vacíos
                    {
                        if (regexNumero.IsMatch(txtMayor.Text) && regexNumero.IsMatch(txtMenor.Text))//Valido si el dato es un número
                        {
                            //En dado caso todo este correcto retorno un 1 como señal
                            return "1";
                        }
                        else
                        {
                            MessageBox.Show("¡Solo se aceptan números positivos!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡Rellene el campo vacío!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }                    
                    break;

                default:
                    
                    break;
            }
            //En dado caso todo este INCORRECTO retorno un 0 como señal
            return "0";

        }


        private void btnCalculaCuadrado_Click(object sender, EventArgs e)
        {
            string Cuadrado = "Cuadrado";
            if(valida(Cuadrado) == "1")//Función para validar que los datos para calcular el area sean números y positivos
            {
                double L = double.Parse(txtLado.Text);//Convierto el dato a double
                double A = 0;

                //En baso al objeto creado de la clase, imboco el metodo y le mando parámetros
                Cuadrado cuadrado = new Cuadrado(A, L);
                cuadrado.CalcularArea(label1);//Según la etiqueta, muestro la respuesta
            }
            
        }

        private void btnCalculaCirculo_Click(object sender, EventArgs e)
        {
            string Circulo = "Circulo";
            if (valida(Circulo) == "1")//Función para validar que los datos para calcular el area sean números y positivos
            {
                double R = double.Parse(txtRadio.Text);//Convierto el dato a double
                double A = 0;

                //En baso al objeto creado de la clase, imboco el metodo y le mando parámetros
                Circulo circulo = new Circulo(A, R);
                circulo.CalcularArea(label2);//Según la etiqueta, muestro la respuesta
            }          
        }

        private void btnCalculaRombo_Click(object sender, EventArgs e)
        {
            string Rombo = "Rombo";
            if (valida(Rombo) == "1")//Validación de datos correctos
            {
                //Convierto los datos a double
                double diagonalMayor = double.Parse(txtMayor.Text);
                double diagonalMenor = double.Parse(txtMenor.Text);
                double A = 0;
                //LLamada del método
                Rombo rombo = new Rombo(A, diagonalMayor, diagonalMenor);
                rombo.CalcularArea(label5);//mostrar respuesta
            }            
        }
    }
}
