﻿
namespace Guia4
{
    partial class G4_Ejercicio_02
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudTotalHorasPasantias = new System.Windows.Forms.NumericUpDown();
            this.nudNota3 = new System.Windows.Forms.NumericUpDown();
            this.nudNota2 = new System.Windows.Forms.NumericUpDown();
            this.nudNota1 = new System.Windows.Forms.NumericUpDown();
            this.nudMateriasInscritas = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNivelEstudio = new System.Windows.Forms.TextBox();
            this.txtNombreProyecto = new System.Windows.Forms.TextBox();
            this.txtCarrera = new System.Windows.Forms.TextBox();
            this.txtCarnet = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtNombreUniversidad = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnRegistrarEstudiante = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnRegresarEstudiante = new System.Windows.Forms.Button();
            this.dgvEstudiante = new System.Windows.Forms.DataGridView();
            this.MenuControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalHorasPasantias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMateriasInscritas)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudiante)).BeginInit();
            this.SuspendLayout();
            // 
            // MenuControl
            // 
            this.MenuControl.Controls.Add(this.tabPage1);
            this.MenuControl.Controls.Add(this.tabPage2);
            this.MenuControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuControl.Location = new System.Drawing.Point(11, 11);
            this.MenuControl.Name = "MenuControl";
            this.MenuControl.SelectedIndex = 0;
            this.MenuControl.Size = new System.Drawing.Size(792, 645);
            this.MenuControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.nudTotalHorasPasantias);
            this.tabPage1.Controls.Add(this.nudNota3);
            this.tabPage1.Controls.Add(this.nudNota2);
            this.tabPage1.Controls.Add(this.nudNota1);
            this.tabPage1.Controls.Add(this.nudMateriasInscritas);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtNivelEstudio);
            this.tabPage1.Controls.Add(this.txtNombreProyecto);
            this.tabPage1.Controls.Add(this.txtCarrera);
            this.tabPage1.Controls.Add(this.txtCarnet);
            this.tabPage1.Controls.Add(this.txtNombre);
            this.tabPage1.Controls.Add(this.txtNombreUniversidad);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.btnRegistrarEstudiante);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(784, 612);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Estudiante de Ingeniería";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(140, 463);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 42;
            this.label3.Text = "Nota3:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(142, 361);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 41;
            this.label2.Text = "Nota1:";
            // 
            // nudTotalHorasPasantias
            // 
            this.nudTotalHorasPasantias.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudTotalHorasPasantias.Location = new System.Drawing.Point(224, 569);
            this.nudTotalHorasPasantias.Maximum = new decimal(new int[] {
            350,
            0,
            0,
            0});
            this.nudTotalHorasPasantias.Name = "nudTotalHorasPasantias";
            this.nudTotalHorasPasantias.Size = new System.Drawing.Size(323, 27);
            this.nudTotalHorasPasantias.TabIndex = 40;
            this.nudTotalHorasPasantias.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudNota3
            // 
            this.nudNota3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudNota3.DecimalPlaces = 1;
            this.nudNota3.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota3.Location = new System.Drawing.Point(224, 463);
            this.nudNota3.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNota3.Name = "nudNota3";
            this.nudNota3.Size = new System.Drawing.Size(100, 27);
            this.nudNota3.TabIndex = 39;
            this.nudNota3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudNota3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudNota2
            // 
            this.nudNota2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudNota2.DecimalPlaces = 1;
            this.nudNota2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota2.Location = new System.Drawing.Point(224, 411);
            this.nudNota2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNota2.Name = "nudNota2";
            this.nudNota2.Size = new System.Drawing.Size(100, 27);
            this.nudNota2.TabIndex = 38;
            this.nudNota2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudNota2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudNota1
            // 
            this.nudNota1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudNota1.DecimalPlaces = 1;
            this.nudNota1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nudNota1.Location = new System.Drawing.Point(224, 358);
            this.nudNota1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudNota1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNota1.Name = "nudNota1";
            this.nudNota1.Size = new System.Drawing.Size(100, 27);
            this.nudNota1.TabIndex = 37;
            this.nudNota1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudNota1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudMateriasInscritas
            // 
            this.nudMateriasInscritas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.nudMateriasInscritas.Location = new System.Drawing.Point(224, 310);
            this.nudMateriasInscritas.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudMateriasInscritas.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMateriasInscritas.Name = "nudMateriasInscritas";
            this.nudMateriasInscritas.Size = new System.Drawing.Size(323, 27);
            this.nudMateriasInscritas.TabIndex = 36;
            this.nudMateriasInscritas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudMateriasInscritas.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 310);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 20);
            this.label1.TabIndex = 35;
            this.label1.Text = "Materias Inscritas:";
            // 
            // txtNivelEstudio
            // 
            this.txtNivelEstudio.Location = new System.Drawing.Point(224, 168);
            this.txtNivelEstudio.Name = "txtNivelEstudio";
            this.txtNivelEstudio.Size = new System.Drawing.Size(323, 27);
            this.txtNivelEstudio.TabIndex = 32;
            this.txtNivelEstudio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNombreProyecto
            // 
            this.txtNombreProyecto.Location = new System.Drawing.Point(224, 520);
            this.txtNombreProyecto.Name = "txtNombreProyecto";
            this.txtNombreProyecto.Size = new System.Drawing.Size(323, 27);
            this.txtNombreProyecto.TabIndex = 29;
            this.txtNombreProyecto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCarrera
            // 
            this.txtCarrera.Location = new System.Drawing.Point(224, 258);
            this.txtCarrera.Name = "txtCarrera";
            this.txtCarrera.Size = new System.Drawing.Size(323, 27);
            this.txtCarrera.TabIndex = 25;
            this.txtCarrera.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCarnet
            // 
            this.txtCarnet.Location = new System.Drawing.Point(224, 121);
            this.txtCarnet.Name = "txtCarnet";
            this.txtCarnet.Size = new System.Drawing.Size(323, 27);
            this.txtCarnet.TabIndex = 24;
            this.txtCarnet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(224, 74);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(323, 27);
            this.txtNombre.TabIndex = 21;
            this.txtNombre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNombreUniversidad
            // 
            this.txtNombreUniversidad.Location = new System.Drawing.Point(224, 210);
            this.txtNombreUniversidad.Name = "txtNombreUniversidad";
            this.txtNombreUniversidad.Size = new System.Drawing.Size(323, 27);
            this.txtNombreUniversidad.TabIndex = 20;
            this.txtNombreUniversidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 569);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(181, 20);
            this.label18.TabIndex = 30;
            this.label18.Text = "Total Horas Pasantías:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(54, 527);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(144, 20);
            this.label17.TabIndex = 28;
            this.label17.Text = "Nombre Proyecto:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(195, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(398, 20);
            this.label10.TabIndex = 27;
            this.label10.Text = "REGISTRO DE ESTUDIANTE DE INGENIERÍA\r\n";
            // 
            // btnRegistrarEstudiante
            // 
            this.btnRegistrarEstudiante.Location = new System.Drawing.Point(655, 551);
            this.btnRegistrarEstudiante.Name = "btnRegistrarEstudiante";
            this.btnRegistrarEstudiante.Size = new System.Drawing.Size(112, 48);
            this.btnRegistrarEstudiante.TabIndex = 26;
            this.btnRegistrarEstudiante.Text = "Registrar";
            this.btnRegistrarEstudiante.UseVisualStyleBackColor = true;
            this.btnRegistrarEstudiante.Click += new System.EventHandler(this.btnRegistrarEstudiante_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(127, 265);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 20);
            this.label11.TabIndex = 19;
            this.label11.Text = "Carrera:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(134, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(64, 20);
            this.label12.TabIndex = 18;
            this.label12.Text = "Carnet:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(140, 414);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 20);
            this.label13.TabIndex = 17;
            this.label13.Text = "Nota2:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(63, 168);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 20);
            this.label14.TabIndex = 16;
            this.label14.Text = "Nivel de Estudio:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(127, 81);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 20);
            this.label15.TabIndex = 15;
            this.label15.Text = "Nombre:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(13, 213);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(189, 20);
            this.label16.TabIndex = 14;
            this.label16.Text = "Nombre de Universidad:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnRegresarEstudiante);
            this.tabPage2.Controls.Add(this.dgvEstudiante);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(784, 612);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Ver Registro Estudiantes";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnRegresarEstudiante
            // 
            this.btnRegresarEstudiante.Location = new System.Drawing.Point(672, 558);
            this.btnRegresarEstudiante.Name = "btnRegresarEstudiante";
            this.btnRegresarEstudiante.Size = new System.Drawing.Size(106, 39);
            this.btnRegresarEstudiante.TabIndex = 2;
            this.btnRegresarEstudiante.Text = "Regresar";
            this.btnRegresarEstudiante.UseVisualStyleBackColor = true;
            this.btnRegresarEstudiante.Click += new System.EventHandler(this.btnRegresarEstudiante_Click);
            // 
            // dgvEstudiante
            // 
            this.dgvEstudiante.AllowUserToAddRows = false;
            this.dgvEstudiante.AllowUserToDeleteRows = false;
            this.dgvEstudiante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEstudiante.Location = new System.Drawing.Point(7, 7);
            this.dgvEstudiante.Name = "dgvEstudiante";
            this.dgvEstudiante.ReadOnly = true;
            this.dgvEstudiante.RowHeadersWidth = 51;
            this.dgvEstudiante.RowTemplate.Height = 24;
            this.dgvEstudiante.Size = new System.Drawing.Size(771, 531);
            this.dgvEstudiante.TabIndex = 0;
            // 
            // G4_Ejercicio_02
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 666);
            this.Controls.Add(this.MenuControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "G4_Ejercicio_02";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Registro de datos";
            this.MenuControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTotalHorasPasantias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudNota1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMateriasInscritas)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEstudiante)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MenuControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudTotalHorasPasantias;
        private System.Windows.Forms.NumericUpDown nudNota3;
        private System.Windows.Forms.NumericUpDown nudNota2;
        private System.Windows.Forms.NumericUpDown nudNota1;
        private System.Windows.Forms.NumericUpDown nudMateriasInscritas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNivelEstudio;
        private System.Windows.Forms.TextBox txtNombreProyecto;
        private System.Windows.Forms.TextBox txtCarrera;
        private System.Windows.Forms.TextBox txtCarnet;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtNombreUniversidad;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnRegistrarEstudiante;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnRegresarEstudiante;
        private System.Windows.Forms.DataGridView dgvEstudiante;
    }
}