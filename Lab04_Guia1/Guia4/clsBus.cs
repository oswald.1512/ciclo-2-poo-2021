﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsBus : clsVehiculo
    {
        public string posicionMotor;//atributo
        public string PosicionMotor //propiedad
        {
            get { return posicionMotor; }
            set { this.posicionMotor = value; }
        }

        public double longitud;//atributo
        public double Longitud //propiedad
        {
            get { return longitud; }
            set { this.longitud = value; }
        }

        //Constructor Bus
        public clsBus()
        {
            this.posicionMotor = "Sin Datos";
            this.longitud = 0;
        }

        //Sobreescritura de métodos virtuales heredados
        public override double ConsumoGas(double capacidad)//Método
        {
            //Redondeo el cálculo del consumo
            double consumo;
            consumo = Math.Round(1 / (capacidad * 4.54), 2);
            return consumo;
        }

        public override string Registrar()//Método
        {

            string imprimirCadena;
            //Creo un formato de string para mostrar la información
            imprimirCadena = "Bus -" + " Cantidad de pasajeros: " + this.CantPasajeros + ", Consumo de gas: " + this.ConsumoGas(this.CapacidadGas) + " litros/Gal" + ", Posicion del motor: " + this.PosicionMotor + ", Tipo de bus: " + this.obtenerClasificacion();

            return imprimirCadena;
        }


        public string obtenerClasificacion()
        {
            string tipo = "";

            if (this.CantPasajeros == 20)
            {
                if (this.Longitud >= 7 || this.Longitud <= 10)
                {
                    tipo = "Buseta";
                }
            }
            else if (this.CantPasajeros == 30)
            {
                if (this.Longitud >= 11 || this.Longitud <= 15)
                {
                    tipo = "AutoBus";
                }
            }
            else if (this.CantPasajeros == 40)
            {
                if (this.Longitud >= 16 || this.Longitud <= 19)
                {
                    tipo = "Bus articulado";
                }
            }
            else if (this.CantPasajeros == 0 && this.Longitud == 0)
            {
                tipo = "Sin Datos";
            }
            else
            {
                tipo = "Normal";
            }

            return tipo;
        }

        //Ejecución de campos vacios
        public string ObtieneDatos(string cantidadPasajeros, string capacidadGasolina, string posicionMotor, string longitudBus)
        {
            //Remplazo de datos
            if (esNumero(cantidadPasajeros.Replace(" ", "")))
                this.CantPasajeros = Convert.ToInt32(cantidadPasajeros);

            if (esNumero(capacidadGasolina.Replace(" ", "")))
                this.CapacidadGas = Convert.ToDouble(capacidadGasolina);

            if (esPalabra(posicionMotor.Replace(" ", "")))
                this.PosicionMotor = posicionMotor;

            if (esNumero(longitudBus.Replace(" ", "")))
                this.Longitud = Convert.ToDouble(longitudBus);

            return "";
        }
    }
}
