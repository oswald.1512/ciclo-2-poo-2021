﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsMoto : clsVehiculo
    {
        public string colorMoto;//atributo
        public string ColorMoto //propiedad
        {
            get { return colorMoto; }
            set { this.colorMoto = value; }
        }

        public int cantLlantas;//atributo
        public int CantLlantas //propiedad
        {
            get { return cantLlantas; }
            set { this.cantLlantas = value; }
        }

        //Constructor 
        public clsMoto()
        {
            this.colorMoto = "Sin Datos";
            this.cantLlantas = 0;
        }


        //Sobreescritura de métodos virtuales heredados
        public override double ConsumoGas(double capacidad)
        {
            double consumo;
            //Redondeo el cálculo del consumo
            consumo = Math.Round(1 / (capacidad * 4.54), 2);
            return consumo;
        }

        public override string Registrar()//Método
        {
            string imprimirCadena;
            //Creo un formato de string para mostrar la información
            imprimirCadena = "Moto -" + " Cantidad de pasajeros: " + this.CantPasajeros + ", Consumo de gas: " + this.ConsumoGas(this.CapacidadGas) + " litros/Gal" + ", Color: " + this.ColorMoto + ", Tipo de moto: " + this.obtenerTipo();

            return imprimirCadena;
        }


        public string obtenerTipo()
        {
            string Tipo = "";

            if (this.cantLlantas == 2 && this.cantPasajeros == 1)
            {
                Tipo = "Carreras";
            }
            else if (this.cantLlantas == 3 && this.cantPasajeros == 4)
            {
                Tipo = "Trimodo";
            }
            else if (this.cantLlantas == 4 && this.cantPasajeros == 2)
            {
                Tipo = "Cuadrimoto";
            }
            else if (this.cantLlantas == 0 && this.cantPasajeros == 0)
            {
                Tipo = "Sin Datos";
            }
            else
            {
                Tipo = "Moto normal";
            }

            return Tipo;
        }

        //Método para campos vacios
        public string ObtieneDatos(string cantidadPasajeros, string capacidadGasolina, string colorVechiculo, string cantidadLlantas)
        {
            //Validación

            if (esNumero(cantidadPasajeros.Replace(" ", "")))
                this.CantPasajeros = Convert.ToInt32(cantidadPasajeros);

            if (esNumero(capacidadGasolina.Replace(" ", "")))
                this.CapacidadGas = Convert.ToDouble(capacidadGasolina);

            if (esPalabra(colorVechiculo.Replace(" ", "")))
                this.ColorMoto = colorVechiculo;

            if (esNumero(cantidadLlantas.Replace(" ", "")))
                this.CantLlantas = Convert.ToInt32(cantidadLlantas);

            return "";
        }
    }
}
