﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsIngenieria : clsUniversitario
    {
        private string nombreProyecto;//atributo
        public string NombreProyecto //propiedad
        {
            get { return nombreProyecto; }
            set { this.nombreProyecto = value; }
        }

        private int totalHorasPasantias;//atributo
        public int TotalHorasPasantias //propiedad
        {
            get { return totalHorasPasantias; }
            set { this.totalHorasPasantias = value; }
        }

        //Constructor clase
        public clsIngenieria()
        {
            this.nombreProyecto = "";
            this.totalHorasPasantias = 0;
        }

        public string ObtieneDatos(string Nombre, string Carnet, string NivelEstudio, string NombreUniversidad, string Carrera, string ManteriasInscritas, string Nota1, string Nota2, string Nota3, string NombreProyecto, string TotalHorasPasantias)
        {
            //Remplazo de datos

            //PONER TODOS LOS ATRIBUTOS

            if (esPalabra(Nombre.Replace(" ", "")))
                this.Nombre = Nombre;

            this.Carnet = Carnet;

            if (esPalabra(NivelEstudio.Replace(" ", "")))
                this.NivelEstudio = NivelEstudio;

            if (esPalabra(NombreUniversidad.Replace(" ", "")))
                this.NombreUniversidad = NombreUniversidad;

            if (esPalabra(Carrera.Replace(" ", "")))
                this.Carrera = Carrera;

            if (esNumero(ManteriasInscritas.Replace(" ", "")))
                this.MateriasInscritas = Convert.ToInt32(ManteriasInscritas);

            if (esNumero(Nota1.Replace(" ", "")) && esNumero(Nota2.Replace(" ", "")) && esNumero(Nota1.Replace(" ", "")))
                this.Notas = Math.Round((Convert.ToDouble(Nota1) + Convert.ToDouble(Nota2) + Convert.ToDouble(Nota3)) / 3, 2);

            if (esNumero(Nota1.Replace(" ", "")) && esNumero(Nota2.Replace(" ", "")) && esNumero(Nota1.Replace(" ", "")))
                //Se divide en la cantidad de unidades valorativas: 4
                this.CUM = Math.Round((Convert.ToDouble(Nota1) + Convert.ToDouble(Nota2) + Convert.ToDouble(Nota3)) / 4, 2);

            if (esPalabra(NombreProyecto.Replace(" ", "")))
                this.NombreProyecto = NombreProyecto;

            if (esNumero(TotalHorasPasantias.Replace(" ", "")))
                this.TotalHorasPasantias = Convert.ToInt32(TotalHorasPasantias);

            return "";
        }
    }
}
