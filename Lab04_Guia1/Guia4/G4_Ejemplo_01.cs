﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Ejemplo_01 : Form
    {
        public G4_Ejemplo_01()
        {
            InitializeComponent();
        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            Sumar suma = new Sumar(); //creacion de objeto
            //llamada del método de la clase
            txtResultado.Text = suma.operar(int.Parse(txtV1.Text), int.Parse(txtV2.Text)).ToString();
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            Restar resta = new Restar(); //creacion de objeto
            //llamada del método de la clase
            txtResultado.Text = resta.operar(int.Parse(txtV1.Text), int.Parse(txtV2.Text)).ToString();
        }

        private void btnMultiplica_Click(object sender, EventArgs e)
        {
            Multiplicar multiplica = new Multiplicar(); //creacion de objeto
            //llamada del método de la clase
            txtResultado.Text = multiplica.operar(int.Parse(txtV1.Text), int.Parse(txtV2.Text)).ToString();
        }

        private void btnLimpia_Click(object sender, EventArgs e)
        {
            txtV1.Clear();
            txtV2.Clear();
            txtResultado.Clear();
        }
    }
}
