﻿
namespace Guia4
{
    partial class G4_Ejercicio_04
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MenuControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cmbCantLlantasMoto = new System.Windows.Forms.ComboBox();
            this.txtColorMoto = new System.Windows.Forms.TextBox();
            this.cmbCantPasajerosMoto = new System.Windows.Forms.ComboBox();
            this.btnRegistraMoto = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCapacidadGasMoto = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cmbCantPuertasCarro = new System.Windows.Forms.ComboBox();
            this.cmbCantPasajerosCarro = new System.Windows.Forms.ComboBox();
            this.btnRegistraCarro = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbTipoCaja = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCapacidadGasCarro = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnVerRegistro = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRegistroBuses = new System.Windows.Forms.Button();
            this.btnRegistroCarros = new System.Windows.Forms.Button();
            this.btnRegistroMotos = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cmbCantPasajerosBus = new System.Windows.Forms.ComboBox();
            this.btnRegistraBus = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbPosicionMotor = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtLongitudBus = new System.Windows.Forms.TextBox();
            this.txtCapacidadGasBus = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.listRegistrado = new System.Windows.Forms.ListBox();
            this.MenuControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuControl
            // 
            this.MenuControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MenuControl.Controls.Add(this.tabPage1);
            this.MenuControl.Controls.Add(this.tabPage2);
            this.MenuControl.Controls.Add(this.tabPage3);
            this.MenuControl.Controls.Add(this.tabPage4);
            this.MenuControl.Controls.Add(this.tabPage5);
            this.MenuControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MenuControl.Location = new System.Drawing.Point(10, 14);
            this.MenuControl.Name = "MenuControl";
            this.MenuControl.SelectedIndex = 0;
            this.MenuControl.Size = new System.Drawing.Size(644, 351);
            this.MenuControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.MenuControl.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cmbCantLlantasMoto);
            this.tabPage1.Controls.Add(this.txtColorMoto);
            this.tabPage1.Controls.Add(this.cmbCantPasajerosMoto);
            this.tabPage1.Controls.Add(this.btnRegistraMoto);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtCapacidadGasMoto);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(636, 318);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Moto";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cmbCantLlantasMoto
            // 
            this.cmbCantLlantasMoto.FormattingEnabled = true;
            this.cmbCantLlantasMoto.Location = new System.Drawing.Point(297, 260);
            this.cmbCantLlantasMoto.Name = "cmbCantLlantasMoto";
            this.cmbCantLlantasMoto.Size = new System.Drawing.Size(121, 28);
            this.cmbCantLlantasMoto.TabIndex = 13;
            // 
            // txtColorMoto
            // 
            this.txtColorMoto.Location = new System.Drawing.Point(297, 197);
            this.txtColorMoto.Name = "txtColorMoto";
            this.txtColorMoto.Size = new System.Drawing.Size(121, 27);
            this.txtColorMoto.TabIndex = 12;
            this.txtColorMoto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cmbCantPasajerosMoto
            // 
            this.cmbCantPasajerosMoto.FormattingEnabled = true;
            this.cmbCantPasajerosMoto.Location = new System.Drawing.Point(297, 78);
            this.cmbCantPasajerosMoto.Name = "cmbCantPasajerosMoto";
            this.cmbCantPasajerosMoto.Size = new System.Drawing.Size(121, 28);
            this.cmbCantPasajerosMoto.TabIndex = 11;
            // 
            // btnRegistraMoto
            // 
            this.btnRegistraMoto.BackColor = System.Drawing.Color.LightGreen;
            this.btnRegistraMoto.Location = new System.Drawing.Point(452, 152);
            this.btnRegistraMoto.Name = "btnRegistraMoto";
            this.btnRegistraMoto.Size = new System.Drawing.Size(99, 49);
            this.btnRegistraMoto.TabIndex = 10;
            this.btnRegistraMoto.Text = "Registrar";
            this.btnRegistraMoto.UseVisualStyleBackColor = false;
            this.btnRegistraMoto.Click += new System.EventHandler(this.btnRegistraMoto_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(197, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(207, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "REGISTRO DE MOTOS";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(29, 260);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Números de llantas:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 197);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Color de fabricación:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(234, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Capacidad de gas en galones:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(194, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Capacidad de pasajeros:";
            // 
            // txtCapacidadGasMoto
            // 
            this.txtCapacidadGasMoto.Location = new System.Drawing.Point(297, 135);
            this.txtCapacidadGasMoto.Name = "txtCapacidadGasMoto";
            this.txtCapacidadGasMoto.Size = new System.Drawing.Size(121, 27);
            this.txtCapacidadGasMoto.TabIndex = 1;
            this.txtCapacidadGasMoto.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.cmbCantPuertasCarro);
            this.tabPage2.Controls.Add(this.cmbCantPasajerosCarro);
            this.tabPage2.Controls.Add(this.btnRegistraCarro);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.cmbTipoCaja);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.txtCapacidadGasCarro);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(636, 318);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Carro";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // cmbCantPuertasCarro
            // 
            this.cmbCantPuertasCarro.FormattingEnabled = true;
            this.cmbCantPuertasCarro.Location = new System.Drawing.Point(297, 197);
            this.cmbCantPuertasCarro.Name = "cmbCantPuertasCarro";
            this.cmbCantPuertasCarro.Size = new System.Drawing.Size(121, 28);
            this.cmbCantPuertasCarro.TabIndex = 22;
            // 
            // cmbCantPasajerosCarro
            // 
            this.cmbCantPasajerosCarro.FormattingEnabled = true;
            this.cmbCantPasajerosCarro.Location = new System.Drawing.Point(297, 78);
            this.cmbCantPasajerosCarro.Name = "cmbCantPasajerosCarro";
            this.cmbCantPasajerosCarro.Size = new System.Drawing.Size(121, 28);
            this.cmbCantPasajerosCarro.TabIndex = 21;
            // 
            // btnRegistraCarro
            // 
            this.btnRegistraCarro.BackColor = System.Drawing.Color.LightGreen;
            this.btnRegistraCarro.Location = new System.Drawing.Point(452, 152);
            this.btnRegistraCarro.Name = "btnRegistraCarro";
            this.btnRegistraCarro.Size = new System.Drawing.Size(99, 49);
            this.btnRegistraCarro.TabIndex = 20;
            this.btnRegistraCarro.Text = "Registrar";
            this.btnRegistraCarro.UseVisualStyleBackColor = false;
            this.btnRegistraCarro.Click += new System.EventHandler(this.btnRegistraCarro_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(225, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(218, 20);
            this.label8.TabIndex = 19;
            this.label8.Text = "REGISTRO DE CARROS";
            // 
            // cmbTipoCaja
            // 
            this.cmbTipoCaja.FormattingEnabled = true;
            this.cmbTipoCaja.Location = new System.Drawing.Point(297, 257);
            this.cmbTipoCaja.Name = "cmbTipoCaja";
            this.cmbTipoCaja.Size = new System.Drawing.Size(121, 28);
            this.cmbTipoCaja.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 260);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "Tipo de caja:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 197);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(164, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Cantidad de puertas:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(25, 135);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(234, 20);
            this.label11.TabIndex = 15;
            this.label11.Text = "Capacidad de gas en galones:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(25, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(194, 20);
            this.label12.TabIndex = 14;
            this.label12.Text = "Capacidad de pasajeros:";
            // 
            // txtCapacidadGasCarro
            // 
            this.txtCapacidadGasCarro.Location = new System.Drawing.Point(297, 135);
            this.txtCapacidadGasCarro.Name = "txtCapacidadGasCarro";
            this.txtCapacidadGasCarro.Size = new System.Drawing.Size(121, 27);
            this.txtCapacidadGasCarro.TabIndex = 12;
            this.txtCapacidadGasCarro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnVerRegistro);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.btnRegistroBuses);
            this.tabPage3.Controls.Add(this.btnRegistroCarros);
            this.tabPage3.Controls.Add(this.btnRegistroMotos);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(636, 318);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Inicio";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnVerRegistro
            // 
            this.btnVerRegistro.BackColor = System.Drawing.Color.LightGreen;
            this.btnVerRegistro.Location = new System.Drawing.Point(249, 234);
            this.btnVerRegistro.Name = "btnVerRegistro";
            this.btnVerRegistro.Size = new System.Drawing.Size(129, 39);
            this.btnVerRegistro.TabIndex = 9;
            this.btnVerRegistro.Text = "Ver Registros";
            this.btnVerRegistro.UseVisualStyleBackColor = false;
            this.btnVerRegistro.Click += new System.EventHandler(this.btnVerRegistro_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(153, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(300, 20);
            this.label2.TabIndex = 8;
            this.label2.Text = "Seleccione el tipo de unidad a registrar";
            // 
            // btnRegistroBuses
            // 
            this.btnRegistroBuses.BackColor = System.Drawing.Color.SkyBlue;
            this.btnRegistroBuses.Location = new System.Drawing.Point(377, 145);
            this.btnRegistroBuses.Name = "btnRegistroBuses";
            this.btnRegistroBuses.Size = new System.Drawing.Size(93, 44);
            this.btnRegistroBuses.TabIndex = 7;
            this.btnRegistroBuses.Text = "Bus";
            this.btnRegistroBuses.UseVisualStyleBackColor = false;
            this.btnRegistroBuses.Click += new System.EventHandler(this.btnRegistroBuses_Click);
            // 
            // btnRegistroCarros
            // 
            this.btnRegistroCarros.BackColor = System.Drawing.Color.SkyBlue;
            this.btnRegistroCarros.Location = new System.Drawing.Point(263, 145);
            this.btnRegistroCarros.Name = "btnRegistroCarros";
            this.btnRegistroCarros.Size = new System.Drawing.Size(93, 44);
            this.btnRegistroCarros.TabIndex = 6;
            this.btnRegistroCarros.Text = "Carro";
            this.btnRegistroCarros.UseVisualStyleBackColor = false;
            this.btnRegistroCarros.Click += new System.EventHandler(this.btnRegistroCarros_Click);
            // 
            // btnRegistroMotos
            // 
            this.btnRegistroMotos.BackColor = System.Drawing.Color.SkyBlue;
            this.btnRegistroMotos.Location = new System.Drawing.Point(157, 145);
            this.btnRegistroMotos.Name = "btnRegistroMotos";
            this.btnRegistroMotos.Size = new System.Drawing.Size(93, 44);
            this.btnRegistroMotos.TabIndex = 5;
            this.btnRegistroMotos.Text = "Moto";
            this.btnRegistroMotos.UseVisualStyleBackColor = false;
            this.btnRegistroMotos.Click += new System.EventHandler(this.btnRegistroMotos_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(259, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "ADUANA";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cmbCantPasajerosBus);
            this.tabPage4.Controls.Add(this.btnRegistraBus);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.cmbPosicionMotor);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.label17);
            this.tabPage4.Controls.Add(this.txtLongitudBus);
            this.tabPage4.Controls.Add(this.txtCapacidadGasBus);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(636, 318);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Bus";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // cmbCantPasajerosBus
            // 
            this.cmbCantPasajerosBus.FormattingEnabled = true;
            this.cmbCantPasajerosBus.Location = new System.Drawing.Point(297, 78);
            this.cmbCantPasajerosBus.Name = "cmbCantPasajerosBus";
            this.cmbCantPasajerosBus.Size = new System.Drawing.Size(121, 28);
            this.cmbCantPasajerosBus.TabIndex = 21;
            // 
            // btnRegistraBus
            // 
            this.btnRegistraBus.BackColor = System.Drawing.Color.LightGreen;
            this.btnRegistraBus.Location = new System.Drawing.Point(452, 152);
            this.btnRegistraBus.Name = "btnRegistraBus";
            this.btnRegistraBus.Size = new System.Drawing.Size(99, 49);
            this.btnRegistraBus.TabIndex = 20;
            this.btnRegistraBus.Text = "Registrar";
            this.btnRegistraBus.UseVisualStyleBackColor = false;
            this.btnRegistraBus.Click += new System.EventHandler(this.btnRegistraBus_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(225, 32);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(203, 20);
            this.label13.TabIndex = 19;
            this.label13.Text = "REGISTRO DE BUSES";
            // 
            // cmbPosicionMotor
            // 
            this.cmbPosicionMotor.FormattingEnabled = true;
            this.cmbPosicionMotor.Location = new System.Drawing.Point(297, 197);
            this.cmbPosicionMotor.Name = "cmbPosicionMotor";
            this.cmbPosicionMotor.Size = new System.Drawing.Size(121, 28);
            this.cmbPosicionMotor.TabIndex = 18;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(25, 263);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(173, 20);
            this.label14.TabIndex = 17;
            this.label14.Text = "Longitud de la unidad:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(25, 200);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(153, 20);
            this.label15.TabIndex = 16;
            this.label15.Text = "Posición del motor:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(25, 135);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(234, 20);
            this.label16.TabIndex = 15;
            this.label16.Text = "Capacidad de gas en galones:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 78);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(194, 20);
            this.label17.TabIndex = 14;
            this.label17.Text = "Capacidad de pasajeros:";
            // 
            // txtLongitudBus
            // 
            this.txtLongitudBus.Location = new System.Drawing.Point(297, 260);
            this.txtLongitudBus.Name = "txtLongitudBus";
            this.txtLongitudBus.Size = new System.Drawing.Size(121, 27);
            this.txtLongitudBus.TabIndex = 13;
            this.txtLongitudBus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtCapacidadGasBus
            // 
            this.txtCapacidadGasBus.Location = new System.Drawing.Point(297, 135);
            this.txtCapacidadGasBus.Name = "txtCapacidadGasBus";
            this.txtCapacidadGasBus.Size = new System.Drawing.Size(121, 27);
            this.txtCapacidadGasBus.TabIndex = 12;
            this.txtCapacidadGasBus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnRegresar);
            this.tabPage5.Controls.Add(this.listRegistrado);
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(636, 318);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Registrados";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnRegresar
            // 
            this.btnRegresar.BackColor = System.Drawing.Color.Khaki;
            this.btnRegresar.Location = new System.Drawing.Point(524, 277);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(106, 35);
            this.btnRegresar.TabIndex = 1;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = false;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // listRegistrado
            // 
            this.listRegistrado.FormattingEnabled = true;
            this.listRegistrado.HorizontalScrollbar = true;
            this.listRegistrado.ItemHeight = 20;
            this.listRegistrado.Location = new System.Drawing.Point(6, 6);
            this.listRegistrado.Name = "listRegistrado";
            this.listRegistrado.Size = new System.Drawing.Size(624, 244);
            this.listRegistrado.TabIndex = 0;
            // 
            // G4_Ejercicio_04
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 378);
            this.Controls.Add(this.MenuControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "G4_Ejercicio_04";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aduana Racing";
            this.MenuControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl MenuControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cmbCantLlantasMoto;
        private System.Windows.Forms.TextBox txtColorMoto;
        private System.Windows.Forms.ComboBox cmbCantPasajerosMoto;
        private System.Windows.Forms.Button btnRegistraMoto;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCapacidadGasMoto;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox cmbCantPuertasCarro;
        private System.Windows.Forms.ComboBox cmbCantPasajerosCarro;
        private System.Windows.Forms.Button btnRegistraCarro;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbTipoCaja;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtCapacidadGasCarro;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnVerRegistro;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRegistroBuses;
        private System.Windows.Forms.Button btnRegistroCarros;
        private System.Windows.Forms.Button btnRegistroMotos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ComboBox cmbCantPasajerosBus;
        private System.Windows.Forms.Button btnRegistraBus;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cmbPosicionMotor;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtLongitudBus;
        private System.Windows.Forms.TextBox txtCapacidadGasBus;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.ListBox listRegistrado;
    }
}