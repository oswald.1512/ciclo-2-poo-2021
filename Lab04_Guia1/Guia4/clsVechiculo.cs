﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsVehiculo //Clase Base
    {
        //Patron de validación para números con expresión regular
        Regex regexNumero = new Regex(@"^[0.0-9.9]+$");

        //cadena de expresiones regulares donde solo se aceptan letras
        Regex regexLetra = new Regex(@"^[a-zA-Z]+$");

        protected int cantPasajeros;//atributo
        public int CantPasajeros //propiedad
        {
            get { return this.cantPasajeros; }
            set { this.cantPasajeros = value; }
        }

        protected double capacidadGas;//atributo
        public double CapacidadGas //propiedad
        {
            get { return capacidadGas; }
            set { this.capacidadGas = value; }
        }

        //Constructor clase base
        public clsVehiculo()
        {
            this.cantPasajeros = 0;
            this.capacidadGas = 0;
        }

        public virtual double ConsumoGas(double capacidad)//Método virtual
        {
            return 0;
        }

        public virtual string Registrar()//Método virtual
        {
            return "";
        }


        //Método de validación sí es número
        public bool esNumero(string numero)
        {
                     
            if (!String.IsNullOrEmpty(numero))
            {
                
                if (regexNumero.IsMatch(numero))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Método de validación sí es letra
        public bool esPalabra(string palabra)
        {
                      
            if (!String.IsNullOrEmpty(palabra))
            {
                
                if (regexLetra.IsMatch(palabra))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
