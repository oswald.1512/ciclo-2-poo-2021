﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;//agregamos esta libreria

namespace Guia4
{
    public class Circulo:Figura
    {
        private double radio; //atributo

        public Circulo(double A, double R):base(A)//constructor con parámetros
        {
            radio = R;
        }

        public double Radio //propiedad
        {
            get { return radio; }
            set { radio = value; }
        }

        //Método sobreescrito
        public override void CalcularArea(Label LR)
        {
            Area = (Math.PI * Math.Pow(Radio, 2));
            LR.Text = "Area: " + Area;
        }
    }
}
