﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;//agregamos esta libreria

namespace Guia4
{
    public class Rombo:Figura
    {
        //Diagonal Mayor
        private double diagonalMayor;
        //Diagonal Menor
        private double diagonalMenor;

        //Constructor de la clase
        public Rombo(double A, double MA, double ME) : base(A)
        {
            diagonalMayor = MA;
            diagonalMenor = ME;
        }

        public double DiagonalMayor
        {
            get { return diagonalMayor; }
            set { diagonalMayor = value; }
        }

        public double DiagonalMenor
        {
            get { return diagonalMenor; }
            set { diagonalMenor = value; }
        }

        //Método agregado
        public override void CalcularArea(Label LR)
        {
            Area = (DiagonalMayor * DiagonalMenor)/2;
            LR.Text = "Area: " + Area;
        }
    }
}
