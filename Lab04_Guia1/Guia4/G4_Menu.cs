﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Menu : Form
    {
        public G4_Menu()
        {
            InitializeComponent();
        }

        private void btnEjercicio1_Click(object sender, EventArgs e)
        {
            Menu Ejercicio1 = new Menu();
            Ejercicio1.Show();
        }

        private void btnEjercicio2_Click(object sender, EventArgs e)
        {
            G4_Ejercicio_02 Ejercicio2 = new G4_Ejercicio_02();
            Ejercicio2.Show();
        }

        private void btnEjercicio3_Click(object sender, EventArgs e)
        {
            G4_Ejemplo_03 Ejercicio3 = new G4_Ejemplo_03();
            Ejercicio3.Show();
        }

        private void btnEjercicio4_Click(object sender, EventArgs e)
        {
            G4_Ejercicio_04 Ejercicio4 = new G4_Ejercicio_04();
            Ejercicio4.Show();
        }
    }
}
