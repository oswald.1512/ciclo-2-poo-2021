﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;//agregamos esta libreria

namespace Guia4
{
    public class Cuadrado:Figura //Clase heredad de figura
    {
        private double lado;//atributo
        
        public Cuadrado(double A, double L) : base(A)//Constructor
        {
            lado = L;
        }

        public double Lado //propiedad
        {
            get { return lado; }
            set { lado = value; }
        }

        //Método sobreescrito
        public override void CalcularArea(Label LR)
        {
            Area = (Lado * Lado);
            LR.Text = "Area: " + Area;
        }
    }
}
