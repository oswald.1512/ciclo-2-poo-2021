﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Ejemplo_04 : Form
    {
        public G4_Ejemplo_04()
        {
            InitializeComponent();
        }

        Figura[] arreglof = new Figura[5];

        private void btnCalcularArreglo_Click(object sender, EventArgs e)
        {
            //inicialización de variables
            double R = double.Parse(txtArreglo.Text);
            double A = 0;
            double L = double.Parse(txtArreglo.Text);

            //Creamos objetos de tipo Figura que pueden ser cuadrados o circulos
            //En el parámetro del valor modificamos en una unidad para que no se vea
            //en todo el mismo resultado
            arreglof[0] = new Cuadrado(A, L);
            arreglof[1] = new Cuadrado(A, L + 1);
            arreglof[2] = new Circulo(A, R);
            arreglof[3] = new Circulo(A, R + 1);
            arreglof[4] = new Circulo(A, R + 2);

            //Cada arreglo envía la respuesta a su label correspondiente
            //El label mostrará en cálculo de acuerdo al objeto que lo invocó
            arreglof[0].CalcularArea(label4);
            arreglof[1].CalcularArea(label5);
            arreglof[2].CalcularArea(label6);
            arreglof[3].CalcularArea(label7);
            arreglof[4].CalcularArea(label8);
        }

        private void btnCalculaCuadrado_Click(object sender, EventArgs e)
        {
            double L = double.Parse(txtLado.Text);
            double A = 0;
            Cuadrado cuadrado = new Cuadrado(A, L);
            cuadrado.CalcularArea(label1);
        }

        private void btnCalculaCirculo_Click(object sender, EventArgs e)
        {
            double R = double.Parse(txtRadio.Text);
            double A = 0;

            Circulo circulo = new Circulo(A, R);
            circulo.CalcularArea(label2);
        }
    }
}
