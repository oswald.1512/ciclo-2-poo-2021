﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Ejercicio_02 : Form
    {
        public G4_Ejercicio_02()
        {
            InitializeComponent();
            //Inicio el tabControl en la página de Inicio
            MenuControl.SelectedTab = tabPage1;

            //función que crea y asigna nombre a las columnas de los dataGridView
            nombresColumnas();
        }

        //Lista que permite tener varios objetos de la clase Estudiante
        private List<clsEstudiante> Estudiantes = new List<clsEstudiante>();

        //Crea y pone nombre a las columans de los dataGridView
        private void nombresColumnas()
        {
            //--------------ESTUDIANTES
            ///Se setea el dataGridView con 5 columnas
            dgvEstudiante.DataSource = null;
            dgvEstudiante.ColumnCount = 10;

            //Los nombres de las columnas que veremos son los de las propiedades
            dgvEstudiante.Columns[0].Name = "Nombre";
            dgvEstudiante.Columns[1].Name = "Carnet";
            dgvEstudiante.Columns[2].Name = "Nivel de Estudio";
            dgvEstudiante.Columns[3].Name = "Nombre de Universidad";
            dgvEstudiante.Columns[4].Name = "Carrera";
            dgvEstudiante.Columns[5].Name = "Materias Inscritas";
            dgvEstudiante.Columns[6].Name = "Promedio de Notas";
            dgvEstudiante.Columns[7].Name = "CUM";
            dgvEstudiante.Columns[8].Name = "NombreProyecto";
            dgvEstudiante.Columns[9].Name = "Total Horas Completadas Pasantías";
        }

        private void limpiaCampos()
        {
            //Campos Estudiantes
            txtNombre.Clear();
            txtCarnet.Clear();
            txtNivelEstudio.Clear();
            txtNombreUniversidad.Clear();
            txtCarrera.Clear();
            nudMateriasInscritas.Value = 1;
            nudNota1.Value = 1;
            nudNota2.Value = 1;
            nudNota3.Value = 1;
            txtNombreProyecto.Clear();
            nudTotalHorasPasantias.Value = 0;
        }

        //Contador en Row[j] para que en cada nueva fila se guarde nuevo registro
        int k = 0;
        private void almacenaEstudiantes()
        {
            //Guardo a través de las propiedades según su tipo de valor
            clsIngenieria estudiaIng = new clsIngenieria();
            estudiaIng.ObtieneDatos(txtNombre.Text, txtCarnet.Text, txtNivelEstudio.Text, txtNombreUniversidad.Text, txtCarrera.Text, nudMateriasInscritas.Value.ToString(), nudNota1.Value.ToString(), nudNota2.Value.ToString(), nudNota3.Value.ToString(), txtNombreProyecto.Text, nudTotalHorasPasantias.Value.ToString());

            //Guardo los objetos creados de la clase en una lista para presentarlos en el dataGridView
            Estudiantes.Add(estudiaIng);
            limpiaCampos();

            do
            {
                dgvEstudiante.Rows.Add(1);//Agrega una fila porque se presentarán nuevos datos
                dgvEstudiante.Rows[k].Cells[0].Value = estudiaIng.Nombre;
                dgvEstudiante.Rows[k].Cells[1].Value = estudiaIng.Carnet;
                dgvEstudiante.Rows[k].Cells[2].Value = estudiaIng.NivelEstudio;
                dgvEstudiante.Rows[k].Cells[3].Value = estudiaIng.NombreUniversidad;
                dgvEstudiante.Rows[k].Cells[4].Value = estudiaIng.Carrera;
                dgvEstudiante.Rows[k].Cells[5].Value = estudiaIng.MateriasInscritas;
                dgvEstudiante.Rows[k].Cells[6].Value = estudiaIng.Notas;
                dgvEstudiante.Rows[k].Cells[7].Value = estudiaIng.CUM;
                dgvEstudiante.Rows[k].Cells[8].Value = estudiaIng.NombreProyecto;
                dgvEstudiante.Rows[k].Cells[9].Value = estudiaIng.TotalHorasPasantias;

                k++;
            } while (k < 1);

            //Lo muevo a la tabPage donde se encuentra su dataGrid
            MenuControl.SelectedTab = tabPage2;
        }

        private void btnRegistrarEstudiante_Click(object sender, EventArgs e)
        {
            //Función que guarda y muestra datos
            almacenaEstudiantes();
        }

        private void btnRegresarEstudiante_Click(object sender, EventArgs e)
        {
            //Muestro tabPage de inicio
            MenuControl.SelectedTab = tabPage1;
        }
    }
}
