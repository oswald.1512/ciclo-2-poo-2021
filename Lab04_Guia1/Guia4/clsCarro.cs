﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guia4
{
    public class clsCarro : clsVehiculo
    {
        public int cantPuertas;//atributo
        public int CantPuertas //propiedad
        {
            get { return cantPuertas; }
            set { this.cantPuertas = value; }
        }

        public string tipoCaja;//atributo
        public string TipoCaja //propiedad
        {
            get { return tipoCaja; }
            set { this.tipoCaja = value; }
        }

        //Constructor Carros
        public clsCarro()
        {
            this.cantPuertas = 0;
            this.tipoCaja = "Sin Datos";
        }


        //Sobreescritura de métodos virtuales heredados
        public override double ConsumoGas(double capacidad)//Método
        {
            
            double consumo;
            consumo = Math.Round(1 / (capacidad * 4.54), 2);
            return consumo;
        }

        public override string Registrar()//Método
        {
            string imprimirCadena;
            //Monstrar datos
            imprimirCadena = "Carro -" + " Cantidad de pasajeros: " + this.CantPasajeros + ", Consumo de gas: " + this.ConsumoGas(this.CapacidadGas) + " litros/Gal" + ", Puertas: " + this.CantPuertas + ", Caja de cambios: " + this.obtenerClasificacion();

            return imprimirCadena;
        }


        public string obtenerClasificacion()
        {
            string clasificado = "";

            if (this.cantPasajeros == 1 && this.cantPuertas == 2)
            {
                clasificado = "Carreras";
            }
            else if (this.cantPasajeros == 2 && this.cantPuertas == 2)
            {
                clasificado = "Deportivos";
            }
            else if (this.cantPasajeros == 5 && this.cantPuertas == 4)
            {
                clasificado = "Familiar";
            }
            else if (this.cantPasajeros == 0 && this.cantPuertas == 0)
            {
                clasificado = "Sin Datos";
            }
            else
            {
                clasificado = "Normal";
            }

            return clasificado;
        }

        //Ejecución de campos vacios
        public string ObtieneDatos(string cantidadPasajeros, string capacidadGasolina, string cantidadPuertas, string caja)
        {
            //Remplazo de datos

            if (esNumero(cantidadPasajeros.Replace(" ", "")))
                this.CantPasajeros = Convert.ToInt32(cantidadPasajeros);

            if (esNumero(capacidadGasolina.Replace(" ", "")))
                this.CapacidadGas = Convert.ToDouble(capacidadGasolina);

            if (esNumero(cantidadPuertas.Replace(" ", "")))
                this.CantPuertas = Convert.ToInt32(cantidadPuertas);

            if (esPalabra(caja.Replace(" ", "")))
                this.TipoCaja = caja;

            return "";
        }
    }
}
