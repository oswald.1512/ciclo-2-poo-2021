﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Guia4
{
    public partial class G4_Ejercicio_04 : Form
    {
        public G4_Ejercicio_04()
        {
            InitializeComponent();
            MenuControl.SelectedTab = tabPage3;
            llenaCombos();
        }

        private void llenaCombos()
        {
            //Llenado de comboBox para la cantidad pasajeros en las Motos
            cmbCantPasajerosMoto.Items.Add("1");
            cmbCantPasajerosMoto.Items.Add("2");
            cmbCantPasajerosMoto.Items.Add("4");

            //Llenado de comboBox para la cantidad de llantas en las Motos
            cmbCantLlantasMoto.Items.Add("2");
            cmbCantLlantasMoto.Items.Add("3");
            cmbCantLlantasMoto.Items.Add("4");

            //Llenado de comboBox para la cantidad pasajeros en los Carros
            cmbCantPasajerosCarro.Items.Add("1");
            cmbCantPasajerosCarro.Items.Add("2");
            cmbCantPasajerosCarro.Items.Add("5");

            //Llenado de comboBox para la cantidad de puertas en los Carros
            cmbCantPuertasCarro.Items.Add("2");
            cmbCantPuertasCarro.Items.Add("4");

            //Llenadoo de comboBox del tipo de caja en los Carros
            cmbTipoCaja.Items.Add("Manual");
            cmbTipoCaja.Items.Add("Automatico");

            //Llenado de comboBox para los pasajeros en los Buses
            cmbCantPasajerosBus.Items.Add("20");
            cmbCantPasajerosBus.Items.Add("30");
            cmbCantPasajerosBus.Items.Add("40");

            //Llenado de comboBox para la posición del motor en los Buses
            cmbPosicionMotor.Items.Add("Atrás");
            cmbPosicionMotor.Items.Add("Delante");
        }
        //Limpieza para todos los campos en los que se introducen datos
        private void limpiaCampos()
        {
            //Campos para las motos
            cmbCantPasajerosMoto.Text = null;
            cmbCantPasajerosMoto.Items.Clear();
            txtCapacidadGasMoto.Clear();
            txtColorMoto.Clear();
            cmbCantLlantasMoto.Text = null;
            cmbCantLlantasMoto.Items.Clear();

            //Campos para los carros
            cmbCantPasajerosCarro.Text = null;
            cmbCantPasajerosCarro.Items.Clear();
            txtCapacidadGasCarro.Clear();
            cmbCantPuertasCarro.Text = null;
            cmbCantPuertasCarro.Items.Clear();
            cmbTipoCaja.Text = null;
            cmbTipoCaja.Items.Clear();

            //Campos para los buses
            cmbCantPasajerosBus.Text = null;
            cmbCantPasajerosBus.Items.Clear();
            txtCapacidadGasBus.Clear();
            cmbPosicionMotor.Text = null;
            cmbPosicionMotor.Items.Clear();
            txtLongitudBus.Clear();

        }

        private void almacenaMotos()
        {
            //Guardo datos
            clsMoto moto = new clsMoto();
            moto.ObtieneDatos(cmbCantPasajerosMoto.Text, txtCapacidadGasMoto.Text, txtColorMoto.Text, cmbCantLlantasMoto.Text);

            //Limpieza y llenado de comboBox
            limpiaCampos();
            llenaCombos();

            //Lista de impresión
            listRegistrado.Items.Add(moto.Registrar());
            MenuControl.SelectedTab = tabPage5;

        }

        private void almacenaCarros()
        {
            //Guardo datos
            clsCarro carro = new clsCarro();
            carro.ObtieneDatos(cmbCantPasajerosCarro.Text, txtCapacidadGasCarro.Text, cmbCantPuertasCarro.Text, cmbTipoCaja.Text);

            //Limpieza y llenado de comboBox
            limpiaCampos();
            llenaCombos();

            //Lista de impresión
            listRegistrado.Items.Add(carro.Registrar());
            MenuControl.SelectedTab = tabPage5;
        }

        private void almacenaBuses()
        {

            //Guardo datos
            clsBus bus = new clsBus();
            bus.ObtieneDatos(cmbCantPasajerosBus.Text, txtCapacidadGasBus.Text, cmbPosicionMotor.Text, txtLongitudBus.Text);

            //Limpieza y llenado de comboBox
            limpiaCampos();
            llenaCombos();

            //Lista de impresión
            listRegistrado.Items.Add(bus.Registrar());
            MenuControl.SelectedTab = tabPage5;
        }

        private void btnRegistroMotos_Click(object sender, EventArgs e)
        {
            //Muevo el tabpage
            MenuControl.SelectedTab = tabPage1;
        }

        private void btnRegistroCarros_Click(object sender, EventArgs e)
        {
            //Muevo el tabpage
            MenuControl.SelectedTab = tabPage2;
        }

        private void btnRegistroBuses_Click(object sender, EventArgs e)
        {
            //Muevo el tabpage
            MenuControl.SelectedTab = tabPage4;
        }

        private void btnVerRegistro_Click(object sender, EventArgs e)
        {
            //Muevo el tabpage
            MenuControl.SelectedTab = tabPage5;
        }

        private void btnRegistraMoto_Click(object sender, EventArgs e)
        {
            //Llamando a la función
            almacenaMotos();
        }

        private void btnRegistraCarro_Click(object sender, EventArgs e)
        {
            //Llamando a la función
            almacenaCarros();
        }

        private void btnRegistraBus_Click(object sender, EventArgs e)
        {
            //Llamando a la función
            almacenaBuses();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            //Regreso al inicial
            MenuControl.SelectedTab = tabPage3;
            limpiaCampos();
            llenaCombos();
        }
    }
}
